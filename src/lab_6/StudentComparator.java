package lab_6;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {

    @Override
    public int compare(Student s1, Student s2) {
        if (s1 == null || s2 == null) throw new NullPointerException();
        return s1.getBirthsday().compareTo(s2.getBirthsday());
    }

}
