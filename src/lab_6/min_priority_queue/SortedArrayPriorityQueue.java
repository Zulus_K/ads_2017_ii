package lab_6.min_priority_queue;

import lab_6.MinPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentArrayList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 * 
 * @author Olena Khomenko
 * 
 *         Sorted array implementation of min-priority queue. <br>
 *         Keeps elements of type Student.<br>
 *         Elements in array are ordered by theirs priorities Use
 *         StudentComparator to compare priorities of Students
 */

public class SortedArrayPriorityQueue implements MinPriorityQueue {

	private StudentArrayList array;
	private StudentComparator c;

	public SortedArrayPriorityQueue(StudentComparator c) {
		this.c = c;
		array = new StudentArrayList();
	}

	@Override
	public void insert(Student s) {
		// TODO
		// add s to the sorted array
	}

	@Override
	public boolean isEmpty() {
		return array.isEmpty();
	}

	@Override
	public Student removeMin() {
		// TODO
		// removes and returns the end of array which is the element with the
		// smallest priority
		// returns null if this queue is empty
		return null;
	}

	@Override
	public Student min() {
		// TODO
		// returns, but does't remove which is the element with the smallest
		// priority
		// returns null if this queue is empty
		return null;
	}

	@Override
	public int size() {
		return array.size();
	}

}
