package lab_6.min_priority_queue;

import lab_6.MinPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentDLList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 * 
 * @author Olena Khomenko
 * 
 *         Double-linked list implementation of min-priority queue. <br>
 *         Keeps elements of type Student.<br>
 *         Elements in list are ordered by theirs priorities Use
 *         StudentComparator to compare priorities of Students
 */

public class SortedListPriorityQueue implements MinPriorityQueue {
	private StudentDLList list;
	private StudentComparator c;

	public SortedListPriorityQueue(StudentComparator c) {
		this.c = c;
		list = new StudentDLList();
	}

	@Override
	public void insert(Student s) {
		// TODO
		// add s to the sorted list
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public Student removeMin() {
		// TODO
		// removes the head of list which keeps the element with the smallest
		// priority and returns this data
		// returns null if this queue is empty
		return null;
	}

	@Override
	public Student min() {
		// TODO
		// Retrieves, but does't remove the head of list which is the element
		// with the smallest priority
		// returns null if this queue is empty
		return null;
	}

	@Override
	public int size() {
		return list.size();
	}

}
