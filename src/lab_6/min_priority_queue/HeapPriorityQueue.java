package lab_6.min_priority_queue;

import lab_6.Heap;
import lab_6.MinPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentArrayList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 *
 * @author Olena Khomenko
 *         <p>
 *         An unbounded min-priority queue based on a priority heap. The
 *         elements of the min-priority queue are ordered by a
 *         StudentComparator. The head of this queue is the least element with
 *         respect to the specified ordering. <br>
 *         A priority queue does not permit null elements. <br>
 *         Keeps elements of type Student.<br>
 */
public class HeapPriorityQueue implements MinPriorityQueue, Heap {
    private StudentArrayList heap;
    private StudentComparator c;

    public HeapPriorityQueue(StudentComparator c) {
        heap = new StudentArrayList();
        this.c = c;
    }

    public int getParent(int child) {
//        float p = child / 2;
//        if (p > (int) p) {
//            p = (int) p -1;
//        }
//        return (int)(p - 1);
        return (child - 1) / 2;
    }

    public int getLeft(int parent) {
        return parent * 2 + 1;
    }

    public int getRight(int parent) {
        return getLeft(parent) + 1;
    }

    @Override
    public void insert(Student s) {
        heap.add(s);
        swim(heap.size() - 1);
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
        for(int i=0; i<heap.size();i++){
            System.out.println(heap.get(i));
        }
    }

    @Override
    public boolean isEmpty() {
        return heap.isEmpty();
    }

    @Override
    public Student removeMin() {
        if (heap.isEmpty()) return null;
        Student min = min();
        if (heap.size() > 1) {
            heap.set(0, heap.get(heap.size() - 1));
            sink(0);
        }
        heap.remove(size() - 1);
        return min;
    }

    @Override
    public Student min() {
        return heap.get(0);
    }

    @Override
    public int size() {
        return heap.size();
    }

    @Override
    public void swim(int index) {
        if (index > 0) {
            int parent = getParent(index);
            if (c.compare(heap.get(parent), heap.get(index)) > 0) {
                swap(parent, index);
                swim(parent);
            }
        }
    }

    private void swap(int a, int b) {
        Student s1 = heap.get(a);
        Student s2 = heap.get(b);
        heap.set(a, s2);
        heap.set(b, s1);
//        heap.set(a, heap.set(b, heap.get(a)));
    }

    @Override
    public void sink(int index) {
        if (index < 0 || index >= heap.size()) return;
        int left = getLeft(index);
        int right = getRight(index);
        int smaller = index;
        if (left < heap.size() && c.compare(heap.get(smaller), heap.get(left)) > 0) {
            smaller = left;
        }
        if (right < heap.size() && c.compare(heap.get(smaller), heap.get(right)) > 0) {
            smaller = right;
        }
        if (smaller != index) {
            swap(smaller, index);
            sink(smaller);
        }
    }


}
