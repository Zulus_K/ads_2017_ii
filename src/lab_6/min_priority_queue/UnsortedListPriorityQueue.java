package lab_6.min_priority_queue;

import lab_6.MinPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentDLList;

import java.util.ArrayList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 *
 * @author Olena Khomenko
 *         <p>
 *         Double-linked list implementation of min-priority queue. <br>
 *         Keeps elements of type Student.<br>
 *         Elements in list are not ordered. To find min-priority element in a
 *         priority queue compare priorities of elements.<br>
 *         Use StudentComparator to compare priorities of Students
 */
public class UnsortedListPriorityQueue implements MinPriorityQueue {
    private StudentDLList list;
    private StudentComparator c;

    public UnsortedListPriorityQueue(StudentComparator c) {
        this.c = c;
        list = new StudentDLList();
    }

    @Override
    public void insert(Student s) {
        list.add(s);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Student removeMin() {
        if (list.isEmpty()) return null;
        // TODO
        // finds, removes and returns from list the student with the smallest
        // priority
        // use StudentComparator to compare priorities of Students
        // returns null if this queue is empty
        Student minStudent = min();
        list.remove(minStudent);
//        int index = 0;
//        for (int i = 1; i < list.size(); i++) {
//            Student currStudent = list.get(i);
//            if (c.compare(minStudent, currStudent) > 0) {
//                minStudent = currStudent;
//                index = i;
//            }
//        }
//        list.remove(index);
        return minStudent;
    }

    @Override
    public Student min() {
        if (list.isEmpty()) return null;
        // TODO
        // finds, retrieves, but does not remove from list the student with the
        // smallest priority
        // use StudentComparator to compare priorities of Students
        // returns null if this queue is empty
        Student minStudent = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            Student currStudent = list.get(i);
            if (c.compare(minStudent, currStudent) > 0) {
                minStudent = currStudent;
            }
        }
        return minStudent;
    }

    @Override
    public int size() {
        return list.size();
    }

//    public String toString() {
//        String s = "UnsortedListPriorityQueue\n";
//        ArrayList<Student> tmpList = new ArrayList(list.size());
//        for (int i = 0; !list.isEmpty(); i++) {
//            Student tmpStud = removeMin();
//            s += tmpStud + "\n";
//            tmpList.add(tmpStud);
//        }
//        while (!tmpList.isEmpty()) {
//            insert(tmpList.remove(0));
//        }
//        return s;
//    }
}
