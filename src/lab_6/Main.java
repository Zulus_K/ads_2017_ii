package lab_6;

import lab_6.min_priority_queue.HeapPriorityQueue;
import lab_6.min_priority_queue.UnsortedListPriorityQueue;

import java.io.File;
import java.util.ArrayList;

public class Main {
    private static String fileName = "students.csv";

    // path to the current directory that contains directory "data"
    private static String currenDir = System.getProperty("user.dir")
            + File.separatorChar + "data_lab6";
    static final int N = 10;

    public static void main(String[] args) {

        // create priority queue based on linear data structure
        // For example,

        MinPriorityQueue pq1 = new UnsortedListPriorityQueue(
                new StudentComparator());

        // create priority queue based on linear data structure
        // For example,
        MinPriorityQueue pq2 = new HeapPriorityQueue(new StudentComparator());

        // read student from file
        // create object of class StudentReader and invoke method read
//        StudentWriter writer = new StudentWriter(fileName);
//        writer.write(N);
        StudentReader readerU = new StudentReader(fileName);
        ArrayList<Student> listU = (ArrayList) readerU.read();

        // fill pq1 and pq2
        for (int i = 0; i < listU.size(); i++) {
            pq1.insert(listU.get(i));
            pq2.insert(listU.get(i));
        }
        System.out.println("======================================================");
        System.out.println("TEST ADD ELEMENTS TO PQ");
        System.out.println("Unsorted LIST queue");
        System.out.println(pq1.size());
        System.out.println("Heap");
        System.out.println(pq2.size());
        System.out.println("======================================================");

        Student[] s1 = new Student[pq1.size()];
        for (int i = 0; i < s1.length; i++) {
            s1[i] = pq1.removeMin();
        }
        System.out.println("======================================================");
        System.out.println("Unsorted LIST queue");
        outputStudents(s1);
        System.out.println("======================================================");

        Student[] s2 = new Student[pq2.size()];
        for (int i = 0; i < s2.length; i++) {
            s2[i] = pq2.removeMin();
        }
        System.out.println("======================================================");
        System.out.println("Heap");
        outputStudents(s2);
        System.out.println("======================================================");
        if (equals(s1, s2) == true) {
            System.out.println("Work is done!");
        }

    }

    private static void outputStudents(Student[] s1) {
        for (Student s : s1) {
            System.out.println(s);
        }
    }

    private static boolean equals(Student[] s1, Student[] s2) {
        for (int i = 0; i < s1.length; i++) {
            if (!s1[i].equals(s2[i])) {
                return false;
            }
        }
        return true;
    }
}
