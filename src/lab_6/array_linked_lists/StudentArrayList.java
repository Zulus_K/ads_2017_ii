package lab_6.array_linked_lists;

import lab_6.Student;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 *
 * @author Olena Khomenko <br>
 *         <p>
 *         Resizable-array. Implements List interface <br>
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 *         <p>
 *         Methods isEmpty, get, set operations run in constant time. <br>
 *         The add operation runs in amortized constant time. <br>
 *         All of the other operations run in linear time (roughly speaking).
 */

public class StudentArrayList implements StudentList {
    // The array buffer into which the elements of the ArrayList are stored. The
    // capacity of the ArrayList is the length of this array buffer

    private Student[] list;

    // Default initial capacity
    private final int defaultCapacity = 10;

    // the size of the array used to store the elements in the list
    private int capacity;

    // The size of the ArrayList (the number of elements it contains)
    private int size;

    /**
     * Constructs an empty list with an initial capacity of ten
     */
    public StudentArrayList() {
        this.capacity = defaultCapacity;
        this.list = new Student[defaultCapacity];
    }

    /**
     * Constructs an empty list with the specified initial capacity
     *
     * @param initialCapacity the initial capacity of the list
     */
    public StudentArrayList(int initialCapacity) {
        this.capacity = initialCapacity;
        this.list = new Student[capacity];
    }

    private void resize() {
        Student[] oldList = list;
        this.capacity += 16;
        this.list = new Student[capacity];
        System.arraycopy(oldList, 0, list, 0, oldList.length);
    }

    @Override
    public boolean add(Student element) {
        return add(size, element);
    }

    @Override
    public boolean add(int index, Student e) {
        if (index < 0 || index > size || e == null) {
            return false;
        }
        if (this.size >= capacity) {
            resize();
        }
        for (int i = size; i > index; i--) {
            list[i] = list[i - 1];
        }
        list[index] = e;
        size++;
        return true;
    }

    @Override
    public Student remove(int index) {
        if (index < 0 || index >= size) return null;
        Student rmStudent = list[index];
        for (int i = index; i < size - 1; i++) {
            list[i] = list[i + 1];
        }
        size--;
        return rmStudent;
    }

    @Override
    public boolean remove(Student element) {
        if (element == null) return false;
        int i = 0;
        //search for index of element
        for (; i < size; i++) {
            if (list[i].equals(element)) {
                break;
            }
        }
        return remove(i) != null;
    }

    @Override
    public Student get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        return list[index];
    }

    @Override
    public Student set(int index, Student element) {
        if (index < 0 || index >= size) {
            return null;
        }
        Student oldVal = list[index];
        list[index] = element;
        return oldVal;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

}
