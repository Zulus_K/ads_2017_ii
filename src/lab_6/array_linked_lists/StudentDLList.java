package lab_6.array_linked_lists;

import lab_6.Student;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 *
 * @author Olena Khomenko <br>
 *         Doubly-linked list of Students. Implements List interface <br>
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 *         <p>
 *         Operations that index into the list will traverse the list from the
 *         beginning or the end, whichever is closer to the specified index. <br>
 */
public class StudentDLList implements StudentList {
    // Pointer to first node
    private DLNode first;

    // Pointer to last node3
    private DLNode last;

    private int size;


    public StudentDLList() {
        this.first = new DLNode(null);
        this.last = new DLNode(null);
        first.next = last;
        last.next = first;
        last.prev = first;
        first.prev = last;
        size = 0;
    }

    private void addAfter(DLNode newNode, DLNode prev) {
        newNode.next = prev.next;
        newNode.prev = prev;
        prev.next.prev = newNode;
        prev.next = newNode;
        size++;
    }

    @Override
    public boolean add(Student element) {
        if (element == null) return false;
        addAfter(new DLNode(element), first);
        return true;
    }

    private DLNode getNode(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        DLNode curr = first.next;
        for (int i = 0; i < index; i++) {
            curr = curr.next;
        }
        return curr;
    }

    @Override
    public boolean add(int index, Student element) {
        if (element == null) return false;
        DLNode prev = getNode(index);
        if (prev == null) return false;
        addAfter(new DLNode(element), prev);
        return true;
    }

    private Student removeNode(DLNode node) {
        Student s = node.data;
        node.next.prev = node.prev;
        node.prev.next = node.next;
        node.next = null;
        node.prev = null;
        size--;
        return s;
    }

    @Override
    public Student remove(int index) {
        DLNode rmNode = getNode(index);
        if (rmNode == null) return null;
        return removeNode(rmNode);
    }

    @Override
    public boolean remove(Student element) {
        if (element == null) return false;
        DLNode rmNode = first.next;
        while (rmNode != last && !rmNode.data.equals(element)) {
            rmNode = rmNode.next;
        }
        if (rmNode == last) return false;
        removeNode(rmNode);
        return true;
    }

    @Override
    public Student get(int index) {
        DLNode node = getNode(index);
        if (node == null) return null;
        return node.data;
    }

    @Override
    public Student set(int index, Student element) {
        DLNode node = getNode(index);
        if (node == null) return null;
        Student s = node.data;
        node.data = element;
        return s;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public String toString() {
        String s = "Linked list\n";
        DLNode curr = first.next;
        while (curr != last) {
            s += curr.data;
            s += "\n";
            curr = curr.next;
        }
        return s;
    }
}

/**
 * Node in a double-linked list.
 */
class DLNode {
    Student data; // the data stored in this node.

    DLNode next; // pointer to the next node
    DLNode prev; // pointer to the previous nod

    /**
     * Construct the node of doubly-linked list with null pointers to the next
     * and previous nodes
     *
     * @param data the data to stored in this node
     */
    public DLNode(Student data) {
        this.data = data;
        next = null;
        prev = null;
    }

    /**
     * Construct the node of doubly-linked list and fill its fields
     *
     * @param data the data to stored in this node
     * @param next pointer to the next node
     * @param prev pointer to the previous node
     */
    public DLNode(Student data, DLNode next, DLNode prev) {
        this.data = data;
        this.next = next;
        this.prev = prev;
    }

}

