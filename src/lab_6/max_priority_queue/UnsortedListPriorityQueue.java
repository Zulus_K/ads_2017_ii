package lab_6.max_priority_queue;

import lab_6.MaxPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentDLList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 * 
 * @author Olena Khomenko
 * 
 *         Double-linked list implementation of max-priority queue. <br>
 *         Keeps elements of type Student.<br>
 *         Elements in list are not ordered. To find max-priority element in a
 *         priority queue compare priorities of elements.<br>
 *         Use StudentComparator to compare priorities of Students
 */

public class UnsortedListPriorityQueue implements MaxPriorityQueue {
	private StudentDLList list;
	private StudentComparator c;

	public UnsortedListPriorityQueue(StudentComparator c) {
		this.c = c;
		list = new StudentDLList();
	}

	@Override
	public void insert(Student s) {
		// TODO
		// add s to the list

	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public Student removeMax() {
		// TODO
		// finds, removes and returns from list the student with the biggest
		// priority
		// use StudentComparator to compare priorities of Students
		// returns null if this queue is empty
		return null;
	}

	@Override
	public Student max() {
		// TODO
		// finds, retrieves, but does not remove from list the student with the
		// biggest priority
		// returns null if this queue is empty
		return null;
	}

	@Override
	public int size() {
		return list.size();
	}

}
