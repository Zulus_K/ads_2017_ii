package lab_6.max_priority_queue;

import lab_6.MaxPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentDLList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 * 
 * @author Olena Khomenko
 * 
 *         Double-linked list implementation of max-priority queue. <br>
 *         Keeps elements of type Student.<br>
 *         Elements in list are ordered by theirs priorities <br>
 *         Use StudentComparator to compare priorities of Students
 */

public class SortedListPriorityQueue implements MaxPriorityQueue {
	private StudentDLList list;
	private StudentComparator c;

	public SortedListPriorityQueue(StudentComparator c) {
		this.c = c;
		list = new StudentDLList();
	}

	@Override
	public void insert(Student s) {
		// TODO
		// add s to the sorted list

	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public Student removeMax() {
		// TODO
		// removes the head of list which keeps the element with the biggest
		// priority and returns this data
		// returns null if this queue is empty
		return null;
	}

	@Override
	public Student max() {
		// TODO
		// Retrieves, but does't remove the head of list which is the element
		// with the biggest priority
		// returns null if this queue is empty
		return null;
	}

	@Override
	public int size() {
		return list.size();
	}

}
