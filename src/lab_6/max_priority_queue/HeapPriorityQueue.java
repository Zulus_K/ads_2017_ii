package lab_6.max_priority_queue;

import lab_6.Heap;
import lab_6.MaxPriorityQueue;
import lab_6.Student;
import lab_6.StudentComparator;
import lab_6.array_linked_lists.StudentArrayList;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 6
 * 
 * @author Olena Khomenko
 * 
 *         An unbounded max-priority queue based on a priority heap. The
 *         elements of the max-priority queue are ordered by a
 *         StudentComparator. The head of this queue is the greatest element
 *         with respect to the specified ordering. <br>
 *         A priority queue does not permit null elements. <br>
 *         Keeps elements of type Student.<br>
 */

public class HeapPriorityQueue implements MaxPriorityQueue, Heap {
	private StudentArrayList heap;
	private StudentComparator c;

	/**
	 * Creates a PriorityQueue with the default initial capacity and whose
	 * elements are ordered according to the specified comparator.
	 * 
	 * @param c
	 *            the comparator that will be used to order this priority queue
	 */

	public HeapPriorityQueue(StudentComparator c) {
		heap = new StudentArrayList();
		this.c = c;
	}

	@Override
	public void insert(Student s) {
		// TODO
		// Inserts the specified element s into heap

	}

	@Override
	public boolean isEmpty() {
		// TODO
		return false;
	}

	@Override
	public Student removeMax() {
		// TODO
		// removes and returns min-priority element of heap
		return null;
	}

	@Override
	public Student max() {
		// TODO
		// returns min-priority element of heap
		return null;
	}

	@Override
	public int size() {
		// TODO
		return 0;
	}

	@Override
	public void swim(int index) {
		// TODO

	}

	@Override
	public void sink(int index) {
		// TODO

	}

	@Override
	public int getParent(int index) {
		// TODO
		return 0;
	}

	@Override
	public int getLeft(int i) {
		// TODO
		return 0;
	}

	@Override
	public int getRight(int i) {
		// TODO
		return 0;
	}

}
