package lab_6;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Zulus on 17.05.2017.
 */
public class StudentWriter {
    private String path;
    public static final String names[] = {"John", "Willson", "Mike", "Joe", "Jack", "Danil", "Leo", "Julia", "Helen", "Ann"};
    public static final String surnames[] = {"Smith", "Robson", "Olimpia", "Jackson", "Tobovski", "Kruchick"};

    public StudentWriter(String fileName, String currenDir) {
        path = Paths.get(currenDir, fileName).toString();

    }

    public StudentWriter(String fileName) {
        String currenDir = System.getProperty("user.dir") + File.separatorChar
                + "data_lab6";
        path = Paths.get(currenDir, fileName).toString();
    }

    private Student getRandomStudent(Random rnd) {
        String name = names[rnd.nextInt(names.length)];
        String surname = surnames[rnd.nextInt(surnames.length)];
        int course = 1 + rnd.nextInt(Student.MAX_COURSE);
        int card = 1 + (int) (rnd.nextDouble() * 20);
        Date birthday = new Date((long) (rnd.nextDouble() * System.currentTimeMillis()));
        return Student.create(name, surname, card, birthday, course);
    }

    private ArrayList<Student> generateRandomStudents(int count) {
        ArrayList<Student> list = new ArrayList<>(count);
        Random rnd = new Random();
        for (int i = 0; i < count; i++) {
            list.add(getRandomStudent(rnd));
        }
        return list;
    }

    private List<String[]> studentToList(ArrayList<Student> students) {
        ArrayList<String[]> list = new ArrayList<>(students.size());
        for (int i = 0; i < students.size(); i++) {
            String[] s = new String[lab5.Student.class.getDeclaredFields().length - 1];
            Student stud = students.get(i);
            s[0] = stud.getName();
            s[1] = stud.getSurname();
            s[2] = Integer.toString(stud.getCardNumber());
            s[3] = lab5.Student.format.format(stud.getBirthsday());
            s[4] = Integer.toString(stud.getCourse());
            list.add(s);
        }
        return list;
    }

    public void write(int count) {
        ArrayList<Student> students = generateRandomStudents(count);
        // read all lines from a file
        List<String[]> lines = studentToList(students);
        try (CSVWriter writer = new CSVWriter(new FileWriter(path))) {
            writer.writeAll(lines);
        } catch (FileNotFoundException e) {
            System.out.println("Error: file  " + path + "   not found");
        } catch (IOException | SecurityException e) {
            System.err.println("Error:read file " + path + "   error");
        }
    }
}
