package list;

/**
 * Created by Zulus on 13.02.2017.
 */
public class SLNode<T> {
    private SLNode<T> next;
    private T data;

    public SLNode(T data) {
        this.data = data;
    }

    public SLNode<T> getNext() {
        return next;
    }

    public void setNext(SLNode<T> next) {
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return data.toString();
    }

    public static SLNode addFirst(SLNode head, SLNode node) {
        node.setNext(head);
        return node;
    }

    public static SLNode getTail(SLNode head) {
        if (head != null) {
            while (head.getNext() != null) {
                head = head.getNext();
            }
        }
        return head;
    }

    public long getLength() {
        SLNode node = this;
        long size = 0;
        while (node != null) {
            size++;
            node = node.next;
        }
        return size;
    }

    public static SLNode getAt(SLNode head, long index) {
        if (index >= head.getLength() || index < 0) return null;
        SLNode node = head;
        for (long i = 1; i <= index; i++) {
            node = node.next;
        }
        return node;
    }
    public static SLNode addLast(SLNode head, SLNode node) {
        if (head == null) return node;
        head = getTail(head);
        head.setNext(node);
        return head;
    }
    public static SLNode addAfter(SLNode head, SLNode node) {
        if (head == null) return node;
        node.setNext(head.getNext());
        head.setNext(node);
        return head;
    }

    public static String listToString(SLNode head) {
        StringBuilder str = new StringBuilder("The SLList{" + getLength(head) + "}: ");
        if (head == null) {
            str.append("is empty");
        } else {
            int i = 0;
            while (head != null) {
                str.append("["+i+"]" + ": " + head + "; ");
                head = head.getNext();
                i++;
            }
        }
        return str.toString();
    }
    public static SLNode removeFirst(SLNode head){
        SLNode node=head.getNext();
        head.setNext(null);
        return node;
    }
    public static SLNode removeLast(SLNode head){
        SLNode node=null;
        if(head!=null ){
            if( head.next!=null) {
                while (head.next.next != null){
                    head=head.getNext();
                }
            }
            node=head.getNext();
            head.setNext(null);
        }
        return node;
    }
    public static int getLength(SLNode head) {
        int len = 0;
        while (head != null) {
            len++;
            head = head.getNext();
        }
        return len;
    }
}
