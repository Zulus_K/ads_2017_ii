package list;

/**
 * Created by Zulus on 13.02.2017.
 */
public class DLNode<T> {
    private T data;
    private DLNode<T> next;
    private DLNode<T> prev;

    /**
     * Transform data to string information
     *
     * @return data as string
     */
    @Override
    public String toString() {
        return data.toString();
    }

    /**
     * get pointer to prev node
     *
     * @return pointer to prev node
     */
    public DLNode<T> getPrev() {
        return prev;
    }

    /**
     * set pointer to prev node
     *
     * @param prev pointer to prev node
     */
    public void setPrev(DLNode<T> prev) {
        this.prev = prev;
    }

    public DLNode(T data) {
        this.data = data;
    }

    /**
     * get data from node
     *
     * @return data
     */
    public T getData() {
        return data;
    }

    /**
     * set data in node
     *
     * @param data data, that will be contained in node
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * return next node
     *
     * @return next node
     */
    public DLNode<T> getNext() {
        return next;
    }

    /**
     * set next node
     *
     * @param next pointer to next element
     */
    public void setNext(DLNode<T> next) {
        this.next = next;
    }

    /**
     * add element at head of list
     *
     * @param head    old head
     * @param newNode a new node
     * @return new head of list
     */
    public static DLNode addFirst(DLNode head, DLNode newNode) {
        newNode.next = head;
        if (head != null) head.prev = newNode;
        return newNode;
    }

    /**
     * add element at tail of list
     *
     * @param head    head of list
     * @param newNode a new node
     * @return new head of list
     */
    public static DLNode addLast(DLNode head, DLNode newNode) {
        if (head == null) {
            head = addFirst(head, newNode);
        } else {
            DLNode tail = getTail(head);
            tail.next = newNode;
            newNode.prev = tail;
        }
        return head;
    }

    /**
     * get last node of list
     *
     * @param head head of list
     * @return tail of list
     */
    public static DLNode getTail(DLNode head) {
        if (head == null) return null;
        while (head.next != null) {
            head = head.next;
        }
        return head;
    }

    /**
     * add new node after node
     *
     * @param head    node, after what we add a new node
     * @param newNode new node
     */
    public static void addAfter(DLNode head, DLNode newNode) {
        if (head != null) {
            newNode.setPrev(head);
            newNode.setNext(head.next);
            if (head.next != null) {
                head.next.setPrev(newNode);
            }
            head.setNext(newNode);
        }
    }

    /**
     * add new node before
     *
     * @param node    head, before what will added new node
     * @param newNode adding node
     * @param newHead head of list
     * @return new head of list
     */
    public static DLNode addBefore(DLNode node, DLNode newNode, DLNode newHead) {
        if (node != null) {
            newNode.next = node;
            newNode.prev = node.prev;
            if (node.prev != null) {
                node.prev.next = newNode;
            } else {
                newHead = newNode;
            }
            node.prev = newNode;
        } else newHead = newNode;
        return newHead;
    }

    /**
     * remove element from list
     *
     * @param head   head of list
     * @param rmNode removing node
     * @return new head of list
     */
    public static DLNode remove(DLNode head, DLNode rmNode) {
        if (rmNode.prev != null) {
            rmNode.prev.next = rmNode.next;
        } else {
            head = head.next;
        }
        if (rmNode.next != null) {
            rmNode.next.prev = rmNode.prev;
        }
        rmNode.next = null;
        rmNode.prev = null;

        return head;
    }

    /**
     * remove first node from list
     *
     * @param head head of list
     * @return a new head of list
     */
    public static DLNode removeFirst(DLNode head) {
        return remove(head, head);
    }

    /**
     * remove last node from list
     *
     * @param head head of list
     * @return head of list
     */
    public static DLNode removeLast(DLNode head) {
        return remove(head, getTail(head));
    }

    /**
     * clear all links to next andprev node
     */
    public void clearLinks() {
        this.next = null;
        this.prev = null;
    }

    /**
     * get count of nodes in list
     *
     * @param head - head of list
     * @return count of nodes
     */
    public static int getLength(DLNode head) {
        int len = 0;
        while (head != null) {
            len++;
            head = head.getNext();
        }
        return len;
    }

    /**
     * Transform DLList to string-form
     *
     * @param head-head of DLList
     * @return String, that contains all nodes of list
     */
    public static String listToString(DLNode head) {
        StringBuilder str = new StringBuilder("The DLList{" + getLength(head) + "}:\n");
        if (head == null) {
            str.append("is empty");
        } else {
            int i = 0;
            while (head != null) {
                str.append("[" + i + "]" + ": " + head + ";\n");
                head = head.getNext();
                i++;
            }
        }
        return str.toString();
    }
}
