package lab3.list;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 3
 *
 * @author Olena Khomenko <br>
 *         Doubly-linked list. Implements List interface <br>
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 *         <p>
 *         Operations that index into the list will traverse the list from the
 *         beginning or the end, whichever is closer to the specified index. <br>
 */
public class DoublyLinkedList implements List<String> {
    private class DLNode<E> {
        E data; // the data stored in this node.

        DLNode next; // pointer to the next node
        DLNode prev; // pointer to the previous nod

        /**
         * Construct the node of doubly-linked list with null pointers to the next
         * and previous nodes
         *
         * @param data the data to stored in this node
         */
        protected DLNode(E data) {
            this.data = data;
            next = null;
            prev = null;
        }

        /**
         * Construct the node of doubly-linked list and fill its fields
         *
         * @param data the data to stored in this node
         * @param next pointer to the next node
         * @param prev pointer to the previous node
         */
        protected DLNode(E data, DLNode next, DLNode prev) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }

    }

    // Pointer to first node
    private DLNode<String> first;
    // Pointer to last node
    private DLNode<String> last;
    private int length;

    private DLNode<String> getNodeAt(int index) {
        DLNode<String> currNode;
        if (index < length / 2) {
            currNode = first.next;
            for (int i = 0; i < index; i++) {
                currNode = currNode.next;
            }
        } else {
            currNode = last.prev;
            for (int i = length - 1; i > index; i--) {
                currNode = currNode.prev;
            }
        }
        return currNode;
    }

    private void insert(String e, int index) {
        DLNode<String> node = new DLNode<String>(e);
        if (index == 0) {
            //add first
            first.next.prev = node;
            node.next = first.next;
            node.prev = first;
            first.next = node;
        } else if (index == length) {
            //add last
            last.prev.next = node;
            node.prev = last.prev;
            node.next = last;
            last.prev = node;
        } else {
            DLNode<String> tmpNode = getNodeAt(index - 1);
            tmpNode.next.prev = node;
            node.next = tmpNode.next;
            node.prev = tmpNode;
            tmpNode.next = node;
        }
        length++;
    }

    private DLNode<String> findNode(String data) {
        DLNode<String> currNode = first.next;
        while (currNode != last && !currNode.data.equals(data)) {
            currNode = currNode.next;
        }
        return currNode;
    }

    private String removeNode(int index) {
        DLNode<String> rmNode = getNodeAt(index);
        rmNode.prev.next = rmNode.next;
        rmNode.next.prev = rmNode.prev;
        rmNode.prev = null;
        rmNode.next = null;
        String data = rmNode.data;
        length--;
        return data;
    }

    private String removeNode(DLNode<String> rmNode) {
        rmNode.prev.next = rmNode.next;
        rmNode.next.prev = rmNode.prev;
        rmNode.prev = null;
        rmNode.next = null;
        String data = rmNode.data;
        length--;
        return data;
    }

    //check, is string contains only  positive integer
    public boolean isValid(String str) {
        if (str == null) return false;
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) >= '0' && str.charAt(i) <= '9')) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean add(String element) {
        if (isValid(element)) {
            insert(element, length);
            return true;
        } else return false;
    }

    @Override
    public boolean add(int index, String element) {
        if (index >= 0 && index <= length && isValid(element)) {
            insert(element, index);
            return true;
        } else return false;
    }

    @Override
    public String removeAt(int index) {
        if (index >= 0 && index < length) {
            return removeNode(index);
        } else return null;
    }

    @Override
    public boolean remove(String element) {
        DLNode<String> rmNode = findNode(element);
        if (rmNode != last) {
            removeNode(rmNode);
            return true;
        } else return false;
    }

    @Override
    public String get(int index) {
        if (index >= 0 && index < length) {
            return getNodeAt(index).data;
        } else return null;
    }

    @Override
    public String set(int index, String element) {
        if (index >= 0 && index < length && isValid(element)) {
            DLNode<String> node = getNodeAt(index);
            String oldData = node.data;
            node.data = element;
            return oldData;
        } else return null;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return length == 0;
    }

    public DoublyLinkedList() {
        this.first = new DLNode<String>(null);
        this.last = new DLNode<String>(null);
        this.length = 0;
        first.next = last;
        first.prev = last;
        last.prev = first;
        last.next = first;
    }

    public String toString() {
        String s = "DoublyLinkedList [" + length + "]:{";
        DLNode<String> curr = first.next;
        for (int i = 0; i < length && curr != last; i++) {
            s += String.format("[%d]\"%s\" ", i, curr.data);
            curr = curr.next;
        }
        s += "}";
        return s;
    }

    public static void test() {
        System.out.println("Start test DoublyLinkedList");
        DoublyLinkedList list = new DoublyLinkedList();
        System.out.println("===================TEST ADD ELEMENTS TO LIST===================");
        {
            int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
            //test add new element and increase list capacity
            for (int val : arr) {
                if (list.add(Integer.toString(val))) {
                    System.out.print(val + " added, ");
                }
            }
            System.out.println();
            System.out.println("Add 11 elements to list");
            System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
            System.out.println(list);
            String val = null;
            if (list.add(val)) {
                System.out.print(val + " added, ");
            } else {
                System.out.println("error: " + val + " didn't add");
            }
            val = "-12";
            if (list.add(val)) {
                System.out.print(val + " added, ");
            } else {
                System.out.println("error: " + val + " didn't add");
            }
            val = "1wedfg";
            if (list.add(val)) {
                System.out.print(val + " added, ");
            } else {
                System.out.println("error: " + val + " didn't add");
            }
            System.out.println(System.lineSeparator());
        }

        //test get element by index
        {
            System.out.println("===================TEST GET ELEMENT BY INDEX===================");
            int index = 0;
            String val = list.get(index);
            System.out.println("Try to get first element");
            System.out.printf("List.size = %d, value %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() - 1;
            val = list.get(index);
            System.out.println("Try to get last element");
            System.out.printf("List.size = %d, value %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() / 2;
            val = list.get(index);
            System.out.println("Try to get middle " + index + " element");
            System.out.printf("List.size = %d, value %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() + 1;
            val = list.get(index);
            System.out.println("Try to get element from out of size");
            System.out.printf("List.size = %d, value %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());
        }

        //test set element by index
        {
            System.out.println("===================TEST SET ELEMENT BY INDEX===================");
            int index = 0;
            System.out.println("Try to set first element");
            if (list.set(index, "444") != null) {
                System.out.printf("List.size = %d\n", list.size());
                System.out.println(list);
                System.out.println(System.lineSeparator());
            } else {
                System.out.println("Error");
            }


            index = list.size() - 1;
            System.out.println("Try to set last element");
            if (list.set(index, "123") != null) {
                System.out.printf("List.size = %d\n", list.size());
                System.out.println(list);
                System.out.println(System.lineSeparator());
            } else {
                System.out.println("Error");
            }

            index = list.size() / 2;
            System.out.println("Try to set middle " + index + " element");
            if (list.set(index, "321") != null) {
                System.out.printf("List.size = %d\n", list.size());
                System.out.println(list);
                System.out.println(System.lineSeparator());
            } else {
                System.out.println("Error");
            }

            index = list.size() + 1;
            System.out.println("Try to set element from out of size");
            if (list.set(index, "321") != null) {
                System.out.printf("List.size = %d\n", list.size());
                System.out.println(list);
                System.out.println(System.lineSeparator());
            } else {
                System.out.println("Error");
            }

            index = list.size();
            System.out.println("Try to set element by irregular data");
            if (list.set(index, "aaaa") != null) {
                System.out.printf("List.size = %d\n", list.size());
                System.out.println(list);
                System.out.println(System.lineSeparator());
            } else {
                System.out.println("Error");
            }
        }
//
        //test remove element by index
        {
            System.out.println("===================TEST REMOVE ELEMENT BY INDEX===================");
            String val;
            val = list.removeAt(0);
            System.out.println("Try to remove first element");
            System.out.printf("List.size = %d, remove %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            val = list.removeAt(list.size() - 1);
            System.out.println("Try to remove last element");
            System.out.printf("List.size = %d, remove %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            val = list.removeAt(list.size() / 2);
            System.out.println("Try to remove middle element");
            System.out.printf("List.size = %d, remove %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            val = list.removeAt(list.size() +1);
            System.out.println("Try to remove element out of bounds");
            System.out.printf("List.size = %d, remove %s\n", list.size(), val);
            System.out.println(list);
            System.out.println(System.lineSeparator());
        }

        //test remove element by value
        {
            System.out.println("===================TEST REMOVE ELEMENT BY VALUE===================");
            String val = "3";
            System.out.println("Try to remove element with val " + val);
            if (list.remove(val)) {
                System.out.printf("List.size = %d, remove %s\n", list.size(), val);
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }

            val = "999";
            System.out.println("Try to remove element with val " + val);
            if (list.remove(val)) {
                System.out.printf("List.size = %d, remove %s\n", list.size(), val);
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }

            val = "-999";
            System.out.println("Try to remove element with  irregular val " + val);
            if (list.remove(val)) {
                System.out.printf("List.size = %d, remove %s\n", list.size(), val);
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }
        }
        //test add element by index
        {
            System.out.println("===================TEST ADD ELEMENT BY INDEX===================");
            String val = "777";
            int index = 0;
            System.out.println("Try to add element " + val + " to head");
            if (list.add(index, val)) {
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }

            index = list.size();
            System.out.println("Try to add element " + val + " to tail");
            if (list.add(index, val)) {
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }

            index = list.size() / 2;
            System.out.println("Try to add element " + val + " to middle position");
            if (list.add(index, val)) {
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }


            index = -1;
            System.out.println("Try to add element " + val + " to out of bounds position");
            if (list.add(index, val)) {
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }else{
                System.out.println("Error");
            }
        }
    }

}
