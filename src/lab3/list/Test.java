package lab3.list;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Zulus on 24.03.2017.
 */
public class Test {
    public static void testSortedInsertion(ArrayList arrayList, DoublyLinkedList linkedList) {
        System.out.println(System.lineSeparator());
        for (int i = 0; i < arrayList.size(); i++) {
            Integer val = arrayList.get(i);
            if (val >= 0 && val % 2 == 0) {
                arrayList.removeAt(i--);
                int j = 0;

                while(j<linkedList.size() && Integer.valueOf(linkedList.get(j)).compareTo(val)>0){
                    j++;
                }
                linkedList.add(j, val.toString());
            }
        }
        System.out.printf("Add elements to linkedList\n");
        System.out.printf("%s\n%s\n", arrayList, linkedList);
        System.out.println(System.lineSeparator());
    }

    public static int[] randomize(int size) {
        Random rnd = new Random(System.currentTimeMillis());
        int data[] = new int[size];
        for (int i = 0; i < size; i++) {
            data[i] = rnd.nextInt(500) - 250;
        }
        return data;
    }

    public static void main(String[] args) {
//        ArrayList.test();
//        DoublyLinkedList.test();
        {
            int[] data = randomize(20);
            ArrayList arrayList = new ArrayList();
            for (int val : data) {
                arrayList.add(val);
            }
            System.out.printf("Add elements to ArrayList:\n%s\nArray: %s", arrayList, Arrays.toString(data));
            DoublyLinkedList linkedList = new DoublyLinkedList();
            testSortedInsertion(arrayList, linkedList);
            System.out.println(System.lineSeparator());
        }

        {
            int[] data = {2,4,6,8};
            ArrayList arrayList = new ArrayList();
            for (int val : data) {
                arrayList.add(val);
            }
            System.out.printf("Add elements to ArrayList:\n%s\nArray: %s", arrayList, Arrays.toString(data));
            DoublyLinkedList linkedList = new DoublyLinkedList();
            testSortedInsertion(arrayList, linkedList);
            System.out.println(System.lineSeparator());
        }
        {
            int[] data = {-1,-2,3,7,9};
            ArrayList arrayList = new ArrayList();
            for (int val : data) {
                arrayList.add(val);
            }
            System.out.printf("Add elements to ArrayList:\n%s\nArray: %s", arrayList, Arrays.toString(data));
            DoublyLinkedList linkedList = new DoublyLinkedList();
            testSortedInsertion(arrayList, linkedList);
            System.out.println(System.lineSeparator());
        }
    }
}
