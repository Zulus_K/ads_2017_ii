package lab3.list;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 3
 *
 * @author Olena Khomenko <br>
 *         <p>
 *         Resizable-array. Implements List interface <br>
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 *         <p>
 *         Methods isEmpty, get, set operations run in constant time. <br>
 *         The add operation runs in amortized constant time. <br>
 *         All of the other operations run in linear time (roughly speaking).
 */

public class ArrayList implements List<Integer> {
    // The array buffer into which the elements of the ArrayList are stored. The
    // capacity of the ArrayList is the length of this array buffer

    private Integer[] list;

    // Default initial capacity
    private final int defaultCapacity = 10;

    // the size of the array used to store the elements in the list
    private int capacity;

    // The size of the ArrayList (the number of elements it contains)
    private int size = 0;

    /**
     * Constructs an empty list with an initial capacity of ten
     */
    public ArrayList() {
        this.capacity = defaultCapacity;
        this.list = new Integer[this.capacity];
    }

    /**
     * Constructs an empty list with the specified initial capacity
     *
     * @param initialCapacity the initial capacity of the list
     */
    public ArrayList(int initialCapacity) {
        this.capacity = initialCapacity;
        this.list = new Integer[this.capacity];
    }

    private void increaseArray() {
        Integer[] newArray = new Integer[this.capacity * 2];
        System.arraycopy(list, 0, newArray, 0, capacity);
        this.capacity *= 2;
        this.list = newArray;
    }


    public boolean add(Integer element) {
        if (this.size >= this.capacity) {
            increaseArray();
        }
        this.list[size++] = element;
        return true;
    }


    public boolean add(int index, Integer e) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Index out of current list's size " + size);
        }
        if (this.size >= this.capacity) {
            increaseArray();
        }

        for (int i = size; i > index; i--) {
            list[i] = list[i - 1];
        }
        list[index] = e;
        size++;
        return true;
    }


    public Integer removeAt(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of current list's size " + size);
        } else {
            int val = list[index];
            int border = size - 1;
            for (int i = index; i < border; i++) {
                list[i] = list[i + 1];
            }
            size--;
            return val;
        }
    }

    public boolean remove(Integer element) {
        int index;
        for (index = 0; index < size; index++) {
            if (list[index] == element) {
                break;
            }
        }
        if (index == size) {
            return false;
        } else {
            removeAt(index);
            return true;
        }
    }


    public Integer get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of current list's size " + size);
        }
        return list[index];
    }


    public Integer set(int index, Integer element) {

        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index out of current list's size " + size);
        }
        int oldVal = list[index];
        list[index] = element;
        return oldVal;
    }


    public int size() {
        return this.size;
    }


    public boolean isEmpty() {
        if (size == 0)
            return true;
        else
            return false;
    }

    public String toString() {
        String s = "ArrayList, size: " + size + " [";
        int bound = size - 1;
        for (int i = 0; i < bound; i++) {
            s += list[i] + ", ";
        }
        if (size != 0) {
            s += list[size - 1];
        }
        s += "}";
        return s;
    }


    public static void test() {
//        ArrayList list = new ArrayList();
        ArrayList list = new ArrayList(1);
        int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        //test add new element and increase list capacity
        for (int val : arr) {
            list.add(val);
        }
        System.out.println("===================TEST ADD ELEMENTS TO LIST===================");
        System.out.println("Add 11 elements to list");
        System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
        System.out.println(list);
        System.out.println(System.lineSeparator());

        //test get element by index
        {
            System.out.println("===================TEST GET ELEMENT BY INDEX===================");
            int index = 0;
            int val = list.get(index);
            System.out.println("Try to get first element");
            System.out.printf("List.size = %d, array.length= %d\n value %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() - 1;
            val = list.get(index);
            System.out.println("Try to get last element");
            System.out.printf("List.size = %d, array.length= %d\n value %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() / 2;
            val = list.get(index);
            System.out.println("Try to get middle element");
            System.out.printf("List.size = %d, array.length= %d\n value %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() + 1;
            try {
                val = list.get(index);
            } catch (Exception e) {
                System.out.println("Catch exception " + e);
            } finally {
                System.out.println("Try to get element from out of size");
            }
            System.out.println(System.lineSeparator());
        }
        //test set element by index
        {
            System.out.println("===================TEST SET ELEMENT BY INDEX===================");
            int index = 0;
            list.set(index, 999);
            System.out.println("Try to set first element");
            System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            index = list.size() - 1;
            list.set(index, 999);
            System.out.println("Try to set last element");
            System.out.println(list);
            System.out.println(System.lineSeparator());


            index = list.size() / 2;
            list.set(index, 999);
            System.out.println("Try to set middle element");
            System.out.println(list);
            System.out.println(System.lineSeparator());


            index = list.size() + 1;
            try {
                list.set(index, 999);
            } catch (Exception e) {
                System.out.println("Catch exception " + e);
            } finally {
                System.out.println("Try to set element from out of size");
                System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
            }
            System.out.println(System.lineSeparator());
        }

        //test remove element by index
        {
            System.out.println("===================TEST REMOVE ELEMENT BY INDEX===================");
            int val;
            val = list.removeAt(0);
            System.out.println("Try to remove first element");
            System.out.printf("List.size = %d, array.length= %d\n remove %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            val = list.removeAt(list.size() - 1);
            System.out.println("Try to remove last element");
            System.out.printf("List.size = %d, array.length= %d\n remove %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());

            val = list.removeAt(list.size() / 2);
            System.out.println("Try to remove middle element");
            System.out.printf("List.size = %d, array.length= %d\n remove %d\n", list.size(), arr.length, val);
            System.out.println(list);
            System.out.println(System.lineSeparator());
        }

        //test remove element by value
        {
            System.out.println("===================TEST REMOVE ELEMENT BY VALUE===================");
            int val = 3;
            if (list.remove(val)) {
                System.out.println("Try to remove element with val " + val);
                System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }
            val = 100;
            if (!list.remove(val)) {
                System.out.println("Try to remove element with val " + val);
                System.out.printf("List.size = %d, array.length= %d\n", list.size(), arr.length);
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }
        }
        //test add element by index
        {
            System.out.println("===================TEST ADD ELEMENT BY INDEX===================");
            int val = 777;
            int index = 0;
            if (list.add(index, val)) {
                System.out.println("Try to add element " + val + " to head");
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }

            index = list.size();
            if (list.add(index, val)) {
                System.out.println("Try to add element " + val + " to tail");
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }

            index = list.size() / 2;
            if (list.add(index, val)) {
                System.out.println("Try to add element " + val + " to middle position");
                System.out.println(list);
                System.out.println(System.lineSeparator());
            }

            try {
                index = -10;
                list.add(index, val);
            } catch (Exception e) {
                System.out.println("Catch exception " + e);
            } finally {
                System.out.println("Try to add element " + val + " to out of border position");
                System.out.println(list);
            }
            System.out.println(System.lineSeparator());

        }
    }

}
