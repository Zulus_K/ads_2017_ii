package lab4.task1;

import lab4.common.Triangle;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestTask1 {

    @Test
    public void testEmptyTable() {
        Hastable tableEmpty = new Hastable(3);
        assertEquals(tableEmpty.size(), 0);
        assertTrue(tableEmpty.isEmpty());
        // remove from empty hashTable
        Triangle gf = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        assertFalse(tableEmpty.remove(gf));
        assertEquals(tableEmpty.size(), 0);
        // search in empty hashTable
        assertFalse(tableEmpty.contains(gf));

    }

    @Test
    public void testAddOneElement() {
        Hastable tableSingle = new Hastable(3);
        Triangle gf = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        assertTrue(tableSingle.add(gf));
        assertTrue(tableSingle.contains(gf));
        assertTrue(tableSingle.remove(gf));
        assertFalse(tableSingle.contains(gf));
    }

    @Test
    public void testAddSomeNotUniqueElements() {
        Hastable tableSingle = new Hastable(3);
        Triangle gf1 = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        Triangle gf2 = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        assertTrue(tableSingle.add(gf1));
        assertFalse(tableSingle.add(gf2));
        assertFalse(tableSingle.add(gf1));
    }

    @Test
    public void testAddSomeUniqueElements() {
        Hastable tableSingle = new Hastable(10);
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 19})));
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 21})));
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 16})));
    }

    @Test
    public void testRehash() {
        Hastable tableSingle = new Hastable(1);
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 19})));
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 21})));
        assertTrue(tableSingle.add(new Triangle(new double[]{0, 0, 1, 0, 0, 16})));
        assertTrue(tableSingle.size()==3);
    }

    @Test
    public void testRemove() {
        Hastable tableSingle = new Hastable();
        Triangle gf1 = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        Triangle gf2 = new Triangle(new double[]{0, 0, 1, 0, 0, 21});
        Triangle gf3 = new Triangle(new double[]{0, 0, 1, 0, 0, 16});

        //test remove one single
        assertTrue(tableSingle.add(gf1));
        assertTrue(tableSingle.remove(gf1));
        //test remove one in not single
        assertTrue(tableSingle.add(gf1));
        assertTrue(tableSingle.add(gf3));
        assertTrue(tableSingle.add(gf2));
        assertTrue(tableSingle.remove(gf2));

        assertFalse(tableSingle.remove(gf2));
        assertFalse(tableSingle.remove(null));
    }

    @Test
    public void testContains() {
        Hastable tableSingle = new Hastable(3);
        Triangle gf1 = new Triangle(new double[]{0, 0, 1, 0, 0, 1});
        Triangle gf2 = new Triangle(new double[]{4, 0, 1, 0, 3, 4});
        System.out.println(gf1+"\n"+gf2);
        assertFalse(gf1.equals(gf2));
        assertTrue(gf1.equals(gf1));
        tableSingle.add(gf1);
        assertTrue(tableSingle.contains(gf1));
        assertFalse(tableSingle.contains(gf2));
    }

}
