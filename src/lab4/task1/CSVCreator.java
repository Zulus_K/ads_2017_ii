package lab4.task1;

import com.opencsv.CSVWriter;
import lab4.common.Triangle;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Zulus on 20.04.2017.
 */
public class CSVCreator {
    private static List<String[]> convertArray(double[][] vertex) {
        List<String[]> list = new ArrayList<>(vertex.length);
        for (int i = 0; i < vertex.length; i++) {
            String s[] = new String[vertex[0].length];
            for (int j = 0; j < vertex[0].length; j++) {
                s[j]=String.valueOf(vertex[i][j]);
            }
            list.add(s);
        }
        return list;
    }

    private static double[][] getRandomVertex(int count) {
        int cntOfNum = Triangle.DIMENSION * Triangle.VERTEX_COUNT;
        double[][] vertex = new double[count][cntOfNum];
        double range = 100;
        Random rnd = new Random(System.nanoTime());

        for (int i = 0; i < count; i++) {
            for (int j = 0; j < cntOfNum; j++) {
                vertex[i][j] = rnd.nextDouble() * range;
            }
        }
        return vertex;
    }

    public static void createCSVFigures(int count, String currenDir, String fileName) {
        double vertex[][] = getRandomVertex(count);
        CSVWriter writer = null;
        String path = Paths.get(currenDir, fileName).toString();
        List<String[]> list = convertArray(vertex);
        try {
            writer = new CSVWriter(new FileWriter(path));
            System.out.println("File \"" + path + " \"  have been writing");
            writer.writeAll(list);
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error: file  " + path + "   not found");
        } catch (IOException e) {
            System.err.println("Error:read file " + path + "   error");
        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }
    }


}
