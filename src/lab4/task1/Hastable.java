package lab4.task1;

import lab4.common.FigureSet;
import lab4.common.Triangle;

/**
 * This class implements a set as a hash table. Hash table is an associative
 * array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * Hash table does not resolve any collisions
 */
public class Hastable implements FigureSet<Triangle> {

    private Triangle[] table;
    /**
     * the number of non-null entries in the hashtable
     */
    private int size;

    /**
     * default size of the hashtable
     */
    private final int initialCapacity = 11;

    /**
     * The load factor is a measure of how full the hash table is allowed to get
     * / before its capacity is automatically increased
     */
    private double loadFactor = 0.75;

    /**
     * It is used in multiplication hash function
     */
    private final double A = (Math.sqrt(5) - 1) / 2;

    /**
     * Constructs a new, empty hashtable with a default initial capacity (11)
     * and load factor (0.75).
     */
    public Hastable() {
        this.table = new Triangle[initialCapacity];
    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * default load factor (0.75).
     *
     * @param initialCapacity the initial capacity of the hashtable
     */
    public Hastable(int initialCapacity) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Irregular initial capacity " + initialCapacity);
        }
        this.table = new Triangle[initialCapacity];
    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * the specified load factor.
     *
     * @param initialCapacity the initial capacity of the hashtable
     * @param loadFactor      the load factor of the hashtable
     */
    public Hastable(int initialCapacity, double loadFactor) {
        this(initialCapacity);
        if (loadFactor > 0 && loadFactor < 1) {
            this.loadFactor = loadFactor;
        } else {
            throw new IllegalArgumentException("Illeregular loadFactor " + loadFactor);
        }

    }

    /**
     * Returns the number of entries in the hashtable
     *
     * @return the number of entries in the hashtable
     */
    public int size() {
        return size;
    }

    /**
     * Increases the capacity of and internally reorganizes this hashtable, in
     * order to accommodate and access its entries more efficiently. This method
     * is called when the number of elements in the hashtable exceeds this
     * hashtable's capacity and load factor
     */
    private void rehash() {
        Triangle[] newTable = new Triangle[table.length + 16];
        for (int i = 0; i < table.length; i++) {
            if (!isEmptySlot(i)) {
                newTable[hash(table[i])] = table[i];
            }
        }
        this.table = newTable;
    }

    private double frac(double val) {
        return val - (long) val;
    }

    /**
     * The hash function is used to calculate the hash value of the object gf.
     * Choose hashing method from your variant (table 1): deletion or
     * multiplication
     *
     * @param gf some geometric figure
     * @return hash value - index in table
     */
    private int hash(Triangle gf) {
        return (int) (table.length * frac(gf.hashCode() * A));
    }

    @Override
    public boolean add(Triangle gf) {
        if (gf == null) return false;
        int ind = hash(gf);
        if (isEmptySlot(ind)) {
            table[ind] = gf;
            size++;
            if ((size / (double) table.length) >= loadFactor) {
                rehash();
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Triangle gf) {
        if (gf == null) return false;
        int ind = hash(gf);
        return !isEmptySlot(ind) && gf.equals(table[ind]);
    }

    @Override
    public boolean remove(Triangle gf) {
        if (gf != null && contains(gf)) {
            int ind = hash(gf);
            table[ind] = null;
            size--;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private boolean isEmptySlot(int ind) {
        return table[ind] == null;
    }

    @Override
    public void print() {
        for (int i = 0; i < table.length; i++) {
            if (!isEmptySlot(i)) {
                System.out.println(String.format(" %-6d|%s", i, table[i]));
            } else {
                System.out.println(String.format(" %-6d|The slot is empty", i, table[i]));
            }
        }
    }

}
