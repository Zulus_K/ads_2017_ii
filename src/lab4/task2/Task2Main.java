package lab4.task2;

import com.opencsv.CSVReader;
import lab4.common.FigureSet;
import lab4.common.Triangle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

//import task1;

public class Task2Main {
    // name of file which stores data about geometric figures
    private static String fileName = "figures1.csv";
    // path to the current directory that contains directory "data"
    private static String currenDir = System.getProperty("user.dir") + File.separatorChar + "data_lab4";

    private static void testH(String fileName) {
        List<String[]> lines = readcsvFile(fileName, currenDir);

        if (!lines.isEmpty()) {
            int numLines = lines.size();
            System.out.println("File contains " + numLines + "  lines");

            // 2) create the array of geometric figures and check data from the
            // file
            Triangle[] farray = createArrayOfFigures(lines);

            // 3) Create a set of figures
            FigureSet figures = new OAHastable(1);
            for (Triangle f : farray) {
                figures.add(f);
            }

            // 4) Print a set of figures
            figures.print();

        } else {
            System.out.println("Error: file  " + fileName + "   is empty");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
//        createCSVFigures(20, currenDir, fileName);
//        testH("figures1.csv");
//        testH("figures2.csv");
        testH("figures3.csv");

    }

    static List<String[]> readcsvFile(String fileName, String dirName) {
        CSVReader reader = null;
        Triangle[] figures = null;
        String path = Paths.get(currenDir, fileName).toString();
        List<String[]> list = null;

        // read data from a file
        try {

            reader = new CSVReader(new FileReader(path));
            System.out.println("File \"" + path + " \"  have been reading ");
            // read all lines from a file
            list = reader.readAll();
            reader.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error: file  " + path + "   not found");
        } catch (IOException e) {
            System.err.println("Error:read file " + path + "   error");
        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }

        return list;
    }

    static Triangle[] createArrayOfFigures(List<String[]> list) {
        int num = 0;

        // create empty array of geometric figures with the length = list.size()
        Triangle[] farray = new Triangle[list.size()];

        // start reading and analyzing each line from this list
        for (Iterator<String[]> it = list.iterator(); it.hasNext(); ) {

            // line contains data about one geometric figures
            String[] line = it.next();

            try {
                // TODO
                // check information about geometric figure and fill
                // fields of Triangle object
                farray[num++] = new Triangle(parseDouble(line));

            } catch (NumberFormatException e) {
                System.out.println(e);
            }

        }
        // check if all data in a file are valid
        if (num != list.size()) {

            // if not, create new array with smaller length
            farray = copyOf(farray, num);
        }

        return farray;
    }

    static Triangle[] copyOf(Triangle[] farray, int num) {
        // TODO
        // Create new array of geometric figures of size num
        // Copy num first elements from farray students to the new array
        // Return new array
        Triangle[] subArray = new Triangle[num];
        System.arraycopy(farray, 0, subArray, 0, num);
        return new Triangle[0];
    }

    static double[] parseDouble(String[] line) {
        double val[] = new double[line.length];
        for (int i = 0; i < line.length; i++) {
            val[i] = Double.valueOf(line[i]);
        }
        return val;
    }

}

