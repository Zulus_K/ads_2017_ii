package lab4.task2;

import lab4.common.Triangle;
import lab4.common.TriangleHashed;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestTask2 {

    @Test
    public void testEmptyTable() {
        OAHastable tableEmpty = new OAHastable(3);
        assertEquals(tableEmpty.size(), 0);
        assertTrue(tableEmpty.isEmpty());

        // remove from empty hashTable
        Triangle gf = new Triangle(new double[]{1, 2, 3, 4, 5, 6});
        assertFalse(tableEmpty.remove(gf));
        assertEquals(tableEmpty.size(), 0);

        // search in empty hashTable
        assertFalse(tableEmpty.contains(gf));
    }

    @Test
    public void testAddOneElement() {
        OAHastable table = new OAHastable(3);
        assertTrue(table.isEmpty());
        Triangle tr1 = new Triangle(new double[]{1, 0, 0, 0, 0, 0});
        assertTrue(table.add(tr1));
        assertEquals(table.size(), 1);
        assertFalse(table.add(tr1));
        assertTrue(table.remove(tr1));
        assertTrue(table.add(tr1));
    }

    @Test
    public void testAddSomeNotUniqueElements() {
        OAHastable table = new OAHastable(3);
        int size = 15;
        for (int i = 0; i < size; i++) {
            assertTrue(table.add(new TriangleHashed(new double[]{1, 0, 0, 0, 0, i})));
        }
        assertEquals(table.size(), size);
        TriangleHashed tr1 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 1});
        assertFalse(table.add(tr1));
        assertEquals(table.size(), size);
    }

    @Test
    public void testAddSomeUniqueElements() {
        OAHastable table = new OAHastable(3);
        int size = 5;
//        for (int i = 0; i < size; i++) {
//            assertTrue(table.add(new TriangleHashed(new double[]{1, 0, 0, 0, 0, i})));
//        }
//        assertEquals(table.size(), size);
        for (int i = 0; i < size; i++) {
            assertTrue(table.add(new Triangle(new double[]{1, 0, 0, 0, 0, i})));
        }
        assertEquals(table.size(), size);
    }

    @Test
    public void testRehash() {
        OAHastable table = new OAHastable(1);
        int size = 20;
        for (int i = 0; i < size; i++) {
            assertTrue(table.add(new Triangle(new double[]{1, 0, 0, 0, 0, i})));
        }
        assertEquals(table.size(), size);
    }

    @Test
    public void testRemove() {
        OAHastable table = new OAHastable(10);
        Triangle tr1 = new Triangle(new double[]{1, 0, 0, 0, 0, 0});
        Triangle tr2 = new Triangle(new double[]{1, 0, 0, 0, 0, 1});
        Triangle tr3 = new Triangle(new double[]{1, 0, 0, 0, 0, 2});
        assertTrue(table.add(tr1));
        assertTrue(table.add(tr2));
        assertTrue(table.add(tr3));
        assertFalse(table.add(tr1));
        assertEquals(table.size(), 3);
        assertTrue(table.remove(tr1));
        assertEquals(table.size(), 2);
        assertTrue(table.add(tr1));
        assertTrue(table.remove(tr3));
//        TriangleHashed trh1 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 0});
//        TriangleHashed trh2 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 1});
//        TriangleHashed trh3 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 2});
//
//        assertTrue(table.add(trh1));
//        assertTrue(table.add(trh2));
//        assertTrue(table.add(trh3));
//        assertFalse(table.add(trh1));
//        assertEquals(table.size(), 6);
//        assertTrue(table.remove(trh1));
//        assertEquals(table.size(), 5);
//        assertTrue(table.add(trh1));
    }

    @Test
    public void testContains() {
        OAHastable table = new OAHastable(4);
        Triangle tr1 = new Triangle(new double[]{1, 0, 0, 0, 0, 0});
        Triangle tr2 = new Triangle(new double[]{1, 0, 0, 0, 0, 1});
        assertFalse(table.contains(tr1));
        table.add(tr1);
        assertTrue(table.contains(tr1));
        assertFalse(table.contains(tr2));
        table.add(tr2);
        table.remove(tr1);
        assertFalse(table.contains(tr1));
//        TriangleHashed trh1 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 0});
//        TriangleHashed trh2 = new TriangleHashed(new double[]{1, 0, 0, 0, 0, 1});
//        assertTrue(trh1.hashCode()==trh2.hashCode());
//        assertFalse(trh1.equals(trh2));
//        assertFalse(table.contains(trh1));
//        table.add(trh1);
//        assertTrue(table.contains(trh1));
//        assertFalse(table.contains(trh2));
//        table.add(trh2);
//        table.remove(trh1);
//        assertFalse(table.contains(trh1));
    }

}
