package lab4.task2;

import lab4.common.FigureSet;
import lab4.common.Triangle;

/**
 * This class implements a set as a hash table. Hash table resolves collisions
 * Hash table is an associative array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 */
public class SChHastable implements FigureSet<Triangle> {

    private DLNode[] table;

    /**
     * the number of non-null entries in the hashtable
     */
    private int size;

    /**
     * default size of the hashtable
     */
    private final int initialCapacity = 11;

    /**
     * The load factor is a measure of how full the hash table is allowed to get
     * / before its capacity is automatically increased
     */
    private double loadFactor = 0.75;

    /**
     * It is used in multiplication hash function
     */
    private final double A = (Math.sqrt(5) - 1) / 2;

    /**
     * Constructs a new, empty hashtable with a default initial capacity (11)
     * and load factor (0.75).
     */
    public SChHastable() {

        // TODO
        // create an array of size equals to default initialCapacity

    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * default load factor (0.75).
     *
     * @param initialCapacity the initial capacity of the hashtable
     */
    public SChHastable(int initialCapacity) {

        // TODO
        // create an array of size equals to initialCapacity
    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * the specified load factor.
     *
     * @param initialCapacity the initial capacity of the hashtable
     * @param loadFactor      the load factor of the hashtable
     */
    public SChHastable(int initialCapacity, double loadFactor) {

        // TODO create an array of size equals to initialCapacity
        // TODO initialize field loadFactor

    }

    /**
     * Returns the number of entries in the hashtable
     *
     * @return the number of entries in the hashtable
     */
    public int size() {

        return size;
    }

    /**
     * Increases the capacity of and internally reorganizes this hashtable, in
     * order to accommodate and access its entries more efficiently. This method
     * is called when the number of elements in the hashtable exceeds this
     * hashtable's capacity and load factor
     */
    private void rehash() {
        // TODO

    }

    /**
     * The hash function is used to calculate the hasvalue of the object gf.
     * Choose hashing method from your variant (table 1): deletion or
     * multiplication
     *
     * @param gf some geometric figure
     * @return hash value - index in table
     */
    private int hash(Triangle gf) {
        // TODO
        //

        return 0;
    }

    public boolean add(Triangle gf) {
        // TODO
        // if gf is not null and hashtable doesn't contain gf
        // ---calculate hash-value (slot number) of gf
        // ---add new Node (gf) to the linked list in the this slot
        // ---increase the size of hash table
        // ---return true
        return false;
    }

    public boolean contains(Triangle gf) {
        // TODO
        // if gf is not null
        // ----Calculate hashvalue (slot number)of gf
        // ------search gf in the linked list in this slot
        return false;
    }

    public boolean remove(Triangle gf) {
        // TODO
        // if gf is not null and hashtable contains gf
        // ---Calculate hashvalue of gf
        // ---if slot is not empty
        // -------delete Node (gf) from linked list
        // -------decrease the size of hash table
        // -------return true

        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO
        // check the size of hashtable
        return false;
    }

    @Override
    public void print() {
        // TODO
        // Output the table , where each row contains a number of the hash-table
        // slot, and the all elements that hashes to this slot.For example,

        // If a slot is empty the row contains a number of the hash-table slot
        // and the message �The slot is empty�.

    }

}

class DLNode {

    private Triangle f;

    private DLNode next;
    private DLNode prev;

}
