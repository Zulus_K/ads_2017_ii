package lab4.task2;

import lab4.common.FigureSet;
import lab4.common.Triangle;

/**
 * This class implements a set as a hash table. Hash table is an associative
 * array of entries. <br>
 * Each entry contains geometric figure or null. <br>
 * Hash table resolves collisions by open addressing
 */
public class OAHastable implements FigureSet<Triangle> {

    private Triangle[] table;

    /**
     * the number of non-null entries in the hashtable
     */
    private int size;

    /**
     * default size of the hashtable
     */
    private final int initialCapacity = 11;

    /**
     * The load factor is a measure of how full the hash table is allowed to get
     * / before its capacity is automatically increased
     */
    private double loadFactor = 0.75;

    /**
     * It is used in multiplication hash function
     */
    private final double A = (Math.sqrt(5) - 1) / 2;

    /**
     * Constructs a new, empty hashtable with a default initial capacity (11)
     * and load factor (0.75).
     */
    private final static DelGeomFigure DELL = DelGeomFigure.getInstance();

    public OAHastable() {
        // create an array of size equals to default initialCapacity
        this.table = new Triangle[initialCapacity];
    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * default load factor (0.75).
     *
     * @param initialCapacity the initial capacity of the hashtable
     */
    public OAHastable(int initialCapacity) {
        // create an array of size equals to initialCapacity
        if (initialCapacity < 1) throw new IllegalArgumentException("Initial size is unvalid");
        this.table = new Triangle[initialCapacity];
    }

    /**
     * Constructs a new, empty hashtable with the specified initial capacity and
     * the specified load factor.
     *
     * @param initialCapacity the initial capacity of the hashtable
     * @param loadFactor      the load factor of the hashtable
     */
    public OAHastable(int initialCapacity, double loadFactor) {
        this(initialCapacity);
        if (loadFactor <= 0 || loadFactor >= 1)
            throw new IllegalArgumentException("Load factor is unvalid " + loadFactor);
        this.loadFactor = loadFactor;
    }

    /**
     * Returns the number of entries in the hashtable
     *
     * @return the number of entries in the hashtable
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Increases the capacity of and internally reorganizes this hashtable, in
     * order to accommodate and access its entries more efficiently. This method
     * is called when the number of elements in the hashtable exceeds this
     * hashtable's capacity and load factor
     */
    private void rehash() {
//        System.out.println("before rehashed " + this);
        Triangle[] oldTable = this.table;
        this.table = new Triangle[oldTable.length + 16];
        for (int i = 0; i < oldTable.length; i++) {
            if (!isEmptySlot(i, oldTable)) {
                addRehashed(oldTable[i]);
            }
        }
//        System.out.println("after rehashed " + this);
    }

    private boolean isEmptySlot(int index, Triangle[] table) {
        return table[index] == null || table[index] == DELL;
    }

    private boolean isEmptySlot(int index) {
        return table[index] == null || table[index] == DELL;
    }

    private double frac(double val) {
        return val - (long) val;
    }

    /**
     * The hash function is used to calculate the hasvalue of the object gf.
     * Choose hashing method from your variant (table 1): deletion or
     * multiplication
     *
     * @param gf some geometric figure
     * @return hash value - index in table
     */
    private int hash(Triangle gf) {
        return gf.hashCode() % table.length;
    }

    private int hash(Triangle gf, int i) {
        return (gf.hashCode() + i) % table.length;
    }

    private void addRehashed(Triangle gf) {
        int probe = 0;
        int index;
        do {
            index = hash(gf, probe);
            if (isEmptySlot(index)) {
                table[index] = gf;
                return;

            } else {
                probe++;
            }
        } while (probe < table.length);
    }

    @Override
    public boolean add(Triangle gf) {
        if (gf != null) {
            if (size / (float) table.length > loadFactor) {
                rehash();
            }
            int probe = 0;
            int index = 0;
            do {
                index = hash(gf, probe);
                if (isEmptySlot(index)) {
                    table[index] = gf;
                    size++;
                    return true;
                } else if (gf.equals(table[index])) {
                    return false;
                } else {
                    probe++;
                }
            } while (probe < table.length);
        }
        return false;
    }

    @Override
    public boolean contains(Triangle gf) {
        if (gf != null) {
            int index;
            int probe = 0;
            do {
                index = hash(gf, probe);
                if (gf.equals(table[index])) {
                    return true;
                } else {
                    probe++;
                }
            } while (probe < table.length && table[index] != null);
            return false;
        }
        return false;
    }

    @Override
    public boolean remove(Triangle gf) {
        if (contains(gf)) {
            int index;
            int probe = 0;
            do {
                index = hash(gf, probe);
                if (table[index].equals(gf)) {
                    table[index] = DELL;
                    size--;
                    return true;
                } else {
                    probe++;
                }
            } while (probe < table.length && table[index] != null);
            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void print() {
        System.out.println("Table size: " + size);
        for (int i = 0; i < table.length; i++) {
            if (!isEmptySlot(i)) {
                System.out.println(String.format(" %-6d|%s", i, table[i]));
            } else {
                System.out.println(String.format(" %-6d|The slot is empty", i, table[i]));
            }
        }
    }

    public String toString() {
        String s = "";
        for (Triangle t : table) {
            if (t == null) {
                s += "0->";
            } else if (t == DELL) {
                s += "D->";
            } else {
                s += "T->";
            }
        }
        return s;
    }
}

/*
 * Represents object that was be deleted from a table
 */

class DelGeomFigure extends Triangle {

    private static DelGeomFigure delFigure = null;

    private DelGeomFigure() {
        super(new double[]{0, 0, 0, 0, 0, 0});
    }

    public static DelGeomFigure getInstance() {
        if (delFigure == null) {
            delFigure = new DelGeomFigure();
        }
        return delFigure;
    }

    @Override
    public String toString() {
        return "deleted element";
    }
}
