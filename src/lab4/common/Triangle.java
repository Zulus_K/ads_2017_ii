package lab4.common;

/**
 * Class represents geometric figure such as rectangle or triangle. Contains
 * coordinates of figure and methods to calculate area and perimeter
 */
public class Triangle {

    private double[][] vertex;
    private double area = -1;
    private double perimetr = -1;
    private int hash = 0;
    public final static int DIMENSION = 2;
    public final static int VERTEX_COUNT = 3;

    private double[][] copyVertex(double[][] val) {
        double[][] dest = new double[val.length][val[0].length];
        for (int i = 0; i < val.length; i++) {
            for (int j = 0; j < val[0].length; j++) {
                dest[i][j] = val[i][j];
            }
        }
        return dest;
    }

    private double[][] copyVertex(double[] val, int count) {
        double[][] dest = new double[val.length][count];
        for (int i = 0, j = 0; j < val.length; i++, j++) {
            dest[i][0] = val[j++];
            dest[i][1] = val[j];
        }
        return dest;
    }

    public Triangle(double[][] vertex) {
        if (vertex.length != VERTEX_COUNT || vertex[0].length != DIMENSION) {
            throw new IllegalArgumentException("Not a triangle's vertex: [" + vertex.length + "][" + vertex[0] + "]");
        }
        this.vertex = copyVertex(vertex);
        this.hash = hashCode();
    }

    public Triangle(double[] vertex) {
        if (vertex.length != VERTEX_COUNT * DIMENSION) {
            throw new IllegalArgumentException("Not a triangle's vertex: [" + vertex.length + "]");
        }
        this.vertex = copyVertex(vertex, DIMENSION);
        this.hash = hashCode();
    }

    public double[] getSides() {
        double sides[] = new double[VERTEX_COUNT];
        sides[0] = Math.sqrt(Math.pow(vertex[0][0] - vertex[1][0], 2) + Math.pow(vertex[0][1] - vertex[1][1], 2));
        sides[1] = Math.sqrt(Math.pow(vertex[2][0] - vertex[1][0], 2) + Math.pow(vertex[2][1] - vertex[1][1], 2));
        sides[2] = Math.sqrt(Math.pow(vertex[0][0] - vertex[2][0], 2) + Math.pow(vertex[0][1] - vertex[2][1], 2));
        return sides;
    }

    public double getArea() {
        if (area < 0) {
            double p = getPerimetr() / 2;
            double sides[] = getSides();
            double k = p;
            for (int i = 0; i < VERTEX_COUNT; i++) {
                k *= p - sides[i];
            }
            area = Math.sqrt(k);
        }
        return area;
    }

    private double getArea(double sides[]) {
        if (area < 0) {
            double p = getPerimetr() / 2;
            double k = p;
            for (int i = 0; i < VERTEX_COUNT; i++) {
                k *= p - sides[i];
            }
            area = Math.sqrt(k);
        }
        return area;
    }

    public double getPerimetr() {
        if (perimetr < 0) {
            perimetr = 0;
            double sides[] = getSides();
            getArea(sides);
            for (int i = 0; i < VERTEX_COUNT; i++) {
                perimetr += sides[i];
            }
        }
        return perimetr;
    }

    /**
     * Returns the hash code value for this geometric figure. The hash code of a
     * figure is defined to be the sum of the hash codes of the elements in the
     * figure, where the hash code of a null element is defined to be zero.
     *
     * @return the hash code value for this object
     */
    public int hashCode() {
        if (hash == 0) {
            hash = 1;
            for (int i = 0; i < VERTEX_COUNT; i++) {
                for (int j = 0; j < DIMENSION; j++) {
                    hash = 31 * hash + (int) vertex[i][j];
                    if (hash < 0) {
                        hash = Math.abs(hash);
                    }
                }
            }
//            hash = hash * 17 + (int) perimetr;
//            hash = hash * 17 + (int) area;
        }
        return hash;
    }

    @Override
    public String toString() {
        String s = String.format("{%-10s: ", hash);
        for (int i = 0; i < VERTEX_COUNT; i++) {
            s += String.format("v%d:[", i);
            s += String.format("%.2f; ", vertex[i][0]);
            s += String.format("%.2f] ", vertex[i][1]);
        }
        s += String.format("}area: %.2f, perimetr: %.2f}", getArea(), getPerimetr());
        return s;
    }

    /**
     * Returns the hash code value for this geometric figure. The hash code of a
     * figure is defined to be the sum of the hash codes of the elements in the
     * figure, where the hash code of a null element is defined to be zero.
     *
     * @return the hash code value for this object
     */
    public boolean equals(Triangle gf) {
        if (gf == null) return false;
        if (this == gf) return true;
        if (gf.hashCode() == this.hashCode()) {
            for (int i = 0; i < VERTEX_COUNT; i++) {
                for (int j = 0; j < DIMENSION; j++) {
                    if (vertex[i][j] != gf.vertex[i][j]) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

}
