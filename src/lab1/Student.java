package lab1;

import static java.lang.Character.isDigit;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 1
 *
 * @author Olena Khomenko
 *         <p>
 *         Represents information about student: its name and number of course <br>
 *         This class is a sample how to define class, fields and methods
 *         <p>
 *         Rewrite this class and its methods <br>
 *         Choose information to be saved in a class from lab manuals (table 1,
 *         col.2).<br>
 *         <p>
 *         Write methods setXXX to set specified value to the field XXX. <br>
 *         <p>
 *         Write method print to output information about student (values of the
 *         fields) in formatted string. <br>
 *         <p>
 *         Write static methods boolean isValidXXX to check whether specified
 *         string could set (or convert and set) to the field XXX
 */

public class Student {
    /**
     * name of this student
     */
    String name;
    /**
     * last name of this student
     */
    String lastName;
    /**
     * number of course (from 1 to 6)
     */
    int course;
    /**
     * score of student (from 1 to 100)
     */
    double score;
    /**
     * Sets the name of a student
     *
     * @param name string specified the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the score of a student
     *
     * @param score average score of student
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * Sets the last name of a student
     *
     * @param lastName string specified the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets the course of a student
     *
     * @param course integer specified the number of student's course
     */
    public void setCourse(int course) {
        this.course = course;
    }

    /**
     * Outputs formatted values of fields in standard output
     */
    public void print() {
        System.out.println(String.format("%-10s| %-15s| %-5.2f| %-6d|", name, lastName, score, course));
    }

    @Override
    public String toString() {
        return String.format("%-10s| %-15s| %-5.2f| %-6d|", name, lastName, score, course);
    }
    /**
     * Determines if the specified string is a student's name. This string is
     * valid if it contains all alphabet letters and begins from uppercase
     * letter
     *
     * @param name the string to be tested
     * @return true if the specified string is a name, false otherwise.
     */
    public static boolean isValidName(String name) {
        if (!Character.isUpperCase(name.charAt(0))) {
            return false;
        }
        for (int i = 1; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i)))
                return false;
        }
        return true;

    }

    /**
     * Determines if the specified string is a student's last name. This string is
     * valid if it contains all alphabet letters and begins from uppercase
     * letter
     *
     * @param lastName the string to be tested
     * @return true if the specified string is a name, false otherwise.
     */
    public static boolean isValidLastName(String lastName) {
        return isValidName(lastName);
    }

    /**
     * Determines if the specified string is number of course. This string is
     * valid if it contains only one digit character: '1', '2', '3', '4', '5' or
     * '6'
     *
     * @param courseStr the string to be tested
     * @return true if the specified string is a number of course, false
     * otherwise.
     */
    public static boolean isValidCourseNumber(String courseStr) {
        if (courseStr.length() == 1) {
            char ch = courseStr.charAt(0);
            if (ch >= '1' && ch <= '6') {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines if the specified string is number of course. This string is
     * valid if it contains only one digit character: '0'-'9', and '.'
     *
     * @param scoreStr the string to be tested
     * @return true if the specified string is a score of student, false
     * otherwise.
     */
    public static boolean isValidScore(String scoreStr) {
        final double maxScore=100;
        final double minScore=1;
        boolean isDotPresent = false;
        for (int i = 0; i < scoreStr.length(); i++) {
            if (scoreStr.charAt(i) == '.' && !isDotPresent) {
                isDotPresent = true;
            } else if (!isDigit(scoreStr.charAt(i))) {
                return false;
            }
        }
        double score = Double.parseDouble(scoreStr);
        if (score < maxScore && score > minScore)
            return true;
        else return false;
    }

}
