package lab1;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Zulus on 08.02.2017.
 */
public class CSVCreator {
    // name of file which stores data about students
    private static String fileName = "students.csv";
    // path to the current directory that contains directory "data"
    private static String currenDir = System.getProperty("user.dir")
            + File.separatorChar + "data_lab1";
    static String[] names={"Danil","Alexandr","Maxim","George","Vladimir","Ruslan","Elena","Ann","Tatiana","Roman"};
    static String[] lastNames={"Karpenko","Muhin","Tutchev","Romin","Yaromin","Zgurovskiy","Mikituk","Andrievskiy","Polyoviy","Nemec"};
    static final int minCourse=1;
    static final int maxCourse=6;
    static final double minScore=1;
    static final double maxScore=100;

    public static void writeDataToFile(String path,int count){
        CSVWriter writer = null;
        Student[] students = null;
        try {
            writer = new CSVWriter(new FileWriter(path));
            System.out.println("File \"" + path + " \"  have been opened ");
            writer.writeAll(createStudentsList(count));
            writer.close();
            System.out.println("File \"" + path + " \"  have been closed ");
        } catch (FileNotFoundException e) {
            System.out.println("Error: file  " + path + "   not found");
        } catch (IOException e) {
            System.err.println("Error:read file " + path + "   error");
        } catch (SecurityException e) {
            System.err.println(e.getMessage());
        }
    }

    public static List<String[]> createStudentsList(int count){
        ArrayList<String[]> list=new ArrayList<>(count);
        Random rand=new Random(System.currentTimeMillis());
        for(int i=0; i<count; i++){
            list.add(getRandomStudentStr(rand));
        }
        return list;
    }

    public static String[] getRandomStudentStr(Random rand){
        String[] fields={
                names[rand.nextInt(names.length)],
                lastNames[rand.nextInt(lastNames.length)],
                String.valueOf((minScore+rand.nextDouble()*maxScore)).substring(0,4),
                String.valueOf((minCourse+rand.nextInt(maxCourse))),
        };
        return fields;
    }

    public static void main(String[] args) {
        writeDataToFile(Paths.get(currenDir, "test_6.csv").toString(),10);
    }
}
