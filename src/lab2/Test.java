package lab2;

/**
 * Created by Zulus on 01.03.2017.
 */
public class Test {
    private long timeStart;
    private long timeEnd;
    private int elementCount;

    public Test(int elementCount) {
        timeStart = -1;
        timeEnd = -1;
        this.elementCount = elementCount;
    }

    public void start() {
        this.timeStart = System.nanoTime();
    }

    public void end() {
        this.timeEnd = System.nanoTime();
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getElementCount() {
        return elementCount;
    }

    public void setElementCount(int elementCount) {
        this.elementCount = elementCount;
    }

    public long getTimeDiff() {
        return timeEnd - timeStart;
    }

    public String toString() {
        return elementCount + "," + getTimeDiff();
    }
}
