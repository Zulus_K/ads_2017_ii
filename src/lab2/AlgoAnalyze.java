package lab2;

import list.SLNode;

import java.io.*;
import java.nio.file.Paths;

/**
 * Created by Zulus on 01.03.2017.
 */
public class AlgoAnalyze {
    private static String dataPath = Paths.get(System.getProperty("user.dir") + "/data_lab2").toString();

    public static void readIntArrayFromFile(String filePath, int[] data) {
        try (BufferedReader bf = new BufferedReader(new FileReader(filePath))) {
            String s;
            int i = 0;
            while ((s = bf.readLine()) != null && i < data.length) {
                data[i++] = Integer.parseInt(s);
            }
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }
    }

    public static SLNode<Integer> addDataToSLNode(SLNode<Integer> head, int[] data, int startInd, int count) {
        int endInd = (startInd + count) % data.length;
        for (int i = startInd; i < endInd; i++) {
            head = SLNode.addFirst(head, new SLNode(data[i]));
        }
        return head;
    }

    public static SLNode<Integer> SLNodeLinearSearch(SLNode<Integer> head, Integer key) {
        while (head != null && !head.getData().equals(key)) {
            head = head.getNext();
        }
        return head;
    }

    public static Test testLinearSearchSLNode(SLNode<Integer> head, Integer key) {
        Test test = new Test(SLNode.getLength(head));
        test.start();
        SLNodeLinearSearch(head, key);
        test.end();
        return test;
    }

    public static Test test_task1(SLNode<Integer> head, long index) {
        if (index == -1) {

            return testLinearSearchSLNode(head, (Integer) SLNode.getAt(head, head.getLength() - 1).getData() * 2);
        } else {
            return testLinearSearchSLNode(head, (Integer) SLNode.getAt(head, index).getData());
        }
    }


    public static void task1(int iterationCount, int stepCount, int maxLength) {
        String fileOutName = dataPath + "/testTimes_task1.csv";
        int[] data = new int[maxLength + 1];
//        sortArray(data);
        readIntArrayFromFile(dataPath + "/data.txt", data);
        SLNode<Integer> head = null;
        int elementsByStep = maxLength / stepCount;
        int len = 0;
        for (int i = 0; i < stepCount; i++) {
            len = (i + 1) * elementsByStep; //count of elements at list
            System.out.println("Start test#" + i + ": count of elements " + len);
            head = addDataToSLNode(head, data, elementsByStep * i, elementsByStep);
            int midPos = len / 2; //middle element's index
            double[] time = new double[3];
            for (int j = 0; j < iterationCount; j++) {
                time[0] += test_task1(head, 0).getTimeDiff();
                time[1] += test_task1(head, (long) Math.random() * len).getTimeDiff();
                time[2] += test_task1(head, -1).getTimeDiff();
            }
            for (int k = 0; k < 3; k++) {
                time[k] /= iterationCount;
                time[k] /= 1_000_000_000.0;
            }
            try (BufferedWriter bf = new BufferedWriter(new FileWriter(fileOutName, true))) {
                bf.append(String.format("\"%d\", \"%.10f\", \"%.10f\", \"%.10f\"\n", len, time[0], time[1], time[2]));
            } catch (IOException e) {
                System.out.println("IO exception " + e);
            }
        }

    }

    public static void main(String[] args) {
        writeDataToFile(dataPath + "/data.txt", 10_000_001, -5000_000);
//        task1(10, 1000, 10_000_000);
    }

    public static void writeDataToFile(String fileName, int count, int min) {
//        Random rnd=new Random(System.nanoTime());
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(fileName))) {
            for (; count > 0; count--) {
                bf.write((min + count) + "\n");
            }
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }
    }
}
