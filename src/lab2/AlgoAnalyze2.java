package lab2;

import java.io.*;
import java.nio.file.Paths;
import java.util.Random;

/**
 * Created by Zulus on 09.03.2017.
 */
public class AlgoAnalyze2 {
    private static String dataPath = Paths.get(System.getProperty("user.dir") + "/data_lab2").toString();

    public static void main(String[] args) {
        writeDataToFile(dataPath + "/data2.txt", 1_000_001, 500_000);
        task2(5, 500, 500_000);
    }

    public static void readIntArrayFromFile(String filePath, int[] data) {
        try (BufferedReader bf = new BufferedReader(new FileReader(filePath))) {
            String s;
            int i = 0;
            while ((s = bf.readLine()) != null && i < data.length) {
                data[i++] = Integer.parseInt(s);
            }
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static void reverse(int[] arr) {
        int len = arr.length / 2;
        for (int i = 0; i < len; i++) {
            swap(arr, i, arr.length - 1 - i);
        }
    }

    public static void shuffleArray(int[] arr, int maxIndex) {
        int size = arr.length;
        Random rnd = new Random(System.nanoTime());
        for (int i = maxIndex; i > 1; i--) {
            swap(arr, i - 1, rnd.nextInt(i));
        }
    }

    public static void sortArrayShell(int[] data, int length) {
        int h = 1;
        while (h <= length / 3) {
            h = h * 3 + 1;
        }
        while (h > 0) {
            for (int i = h; i < length; i++) {
                int temp = data[i];
                int j = i;
                while (j > h - 1 && data[j - h] <= temp) {
                    data[j] = data[j - h];
                    j -= h;
                }
                data[j] = temp;
            }
            h = h / 3;
        }
    }

    public static void sortArrayInsertion(int[] data, int length) {
        for (int i = 1; i < length; i++) {
            int j = i - 1;
            int key = data[i];
            while (j >= 0 && data[j] > key) {
                data[j + 1] = data[j];
                j--;
            }
            data[j + 1] = key;
        }
    }

    public static void writeDataToFile(String fileName, int count, int min) {
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(fileName))) {
            Random rnd = new Random(System.nanoTime());
            for (; count > 0; count--) {
                bf.write(rnd.nextInt(min + count) - min + "\n");
            }
        } catch (IOException e) {
            System.out.println("IO exception " + e);
        }
    }

    public static void task2_ShellSort(int[] inData, int maxDataLength, int iterationCount) {
        int data[] = new int[maxDataLength];
        double[] times = new double[3];
        Test test = new Test(maxDataLength);
        System.out.print("Start shell sort:");
        for (int i = 0; i < iterationCount; i++) {
            System.out.print(" c");
            //скопировать входщий массив в рабочий
            System.arraycopy(inData, 0, data, 0, maxDataLength);
            System.out.print(" " + i);
            //отсортировать массив (average time)
            test.start();
            sortArrayShell(data, maxDataLength);
            test.end();
            times[1] += test.getTimeDiff();
            //отсортировать массив в том же порядке(best time)
            test.start();
            sortArrayShell(data, maxDataLength);
            test.end();
            times[0] += test.getTimeDiff();
            //отсортировать массив,отсортированный в противоположном порядке(worth case)
            reverse(data);
            test.start();
            sortArrayShell(data, maxDataLength);
            test.end();
            times[2] += test.getTimeDiff();
            System.out.print("[done],");
        }

        for (int i = 0; i < 3; i++) {
            times[i] /= iterationCount;
            times[i] /= 1_000_000_000.0;
        }
        System.out.printf("\n%d; %.10f; %.10f; %.10f\n", maxDataLength, times[0], times[1], times[2]);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dataPath + "/testTimes_task2_Shell.csv", true))) {
            writer.append(String.format("%d; %.10f; %.10f; %.10f\n", maxDataLength, times[0], times[1], times[2]));
        } catch (IOException e) {
            System.out.println("Shell sort (cnt of el. " + maxDataLength + " ) error write test data " + e);
        }
    }

    public static void task2_InsertionSort(int[] inData, int maxDataLength, int iterationCount) {
        int data[] = new int[maxDataLength];
        double[] times = new double[3];
        Test test = new Test(maxDataLength);
        System.out.print("Start insertion sort:");
        for (int i = 0; i < iterationCount; i++) {
            System.out.print(" c");
            System.arraycopy(inData, 0, data, 0, maxDataLength);
            System.out.print(" " + i);
            //скопировать входщий массив в рабочий
            //отсортировать массив (average time)
            test.start();
            sortArrayInsertion(data, maxDataLength);
            test.end();
            times[1] += test.getTimeDiff();
            //отсортировать массив в том же порядке(best time)
            test.start();
            sortArrayInsertion(data, maxDataLength);
            test.end();
            times[0] += test.getTimeDiff();
            //отсортировать массив,отсортированный в противоположном порядке(worth case)
            reverse(data);
            test.start();
            sortArrayInsertion(data, maxDataLength);
            test.end();
            times[2] += test.getTimeDiff();
            System.out.print("[done],");
        }

        for (int i = 0; i < 3; i++) {
            times[i] /= iterationCount;
            times[i] /= 1_000_000_000.0;
        }
        System.out.printf("\n%d; %.10f; %.10f; %.10f\n", maxDataLength, times[0], times[1], times[2]);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dataPath + "/testTimes_task2_Insertion.csv", true))) {
            writer.append(String.format("%d; %.10f; %.10f; %.10f\n", maxDataLength, times[0], times[1], times[2]));
        } catch (IOException e) {
            System.out.println("Insertion sort (cnt of el. " + maxDataLength + " ) error write test data " + e);
        }
    }

    public static void task2(int iterationCount, int stepCount, int maxLength) {
        int[] allData = new int[maxLength];
        readIntArrayFromFile(dataPath + "/data2.txt", allData);

        int elementsByStep = maxLength / stepCount;
        for (int i = 1; i <= stepCount; i++) {
            System.out.println("Start test #" + i);
            int maxDataLength = i * elementsByStep;
            System.out.println("count of elements " + maxDataLength);
//            shuffleArray(allData, maxDataLength);

//            System.out.println("shuffled");
            task2_ShellSort(allData, maxDataLength, iterationCount);
            task2_InsertionSort(allData, maxDataLength, iterationCount);
            System.out.println();
        }
    }
}
