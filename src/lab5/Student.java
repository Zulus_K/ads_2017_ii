package lab5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 5
 *
 * @author Olena Khomenko <br>
 *         Represents information about student: its name and number of course <br>
 *         This class is a sample how to define class, fields and methods
 *         <p>
 *         Rewrite this class and its methods <br>
 *         Choose information to be saved in a class from lab manuals (table 1,
 *         col.2).<br>
 *         <p>
 *         Write methods setXXX to set specified value to the field XXX. <br>
 *         <p>
 *         Write method print to output information about student (values of the
 *         fields) in formatted string. <br>
 *         <p>
 *         Write static methods boolean isValidXXX to check whether specified
 *         string could set (or convert and set) to the field XXX
 */

public class Student {
    public final static int MAX_COURSE = 6;
    public final static DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
    private int cardNumber;
    private String name;
    private String surname;
    private int course;
    private Date birthsday;

    private Student(String name, String surname, int cardNumber, int course, Date birthsday) {
        setName(name);
        setSurname(surname);
        setCardNumber(cardNumber);
        setBirthsday(birthsday);
        setCourse(course);
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        if (cardNumber > 0)
            this.cardNumber = cardNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        if (course > 0)
            this.course = course;
        else this.course = 1;
    }

    public Date getBirthsday() {
        return birthsday;
    }

    public void setBirthsday(Date birthsday) {
        if (birthsday.before(new Date()))
            this.birthsday = birthsday;
    }

    public int getkey() {
        return cardNumber;
    }

    public static Student create(String name, String surname, int cardNumber, Date birthday, int course) {
        if (isValidName(name) && isValidSurname(surname) && isValidCourse(course) && isValidBirthday(birthday) && isValidCardNumber(cardNumber)) {
            return new Student(name, surname, cardNumber, course, birthday);
        } else {
            return null;
        }
    }

    public static Student create(String name, String surname, String cardNumber, String birthday, String course) {
        if (isValidName(name) &&
                isValidSurname(surname) &&
                isValidCardNumber(cardNumber) &&
                isValidCourse(course) &&
                isValidBirthday(birthday)) {
            try {
                return new Student(name,
                        surname,
                        Integer.parseInt(cardNumber),
                        Integer.parseInt(course),
                        format.parse(birthday));
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return String.format("name: %-10s, surname: %-10s, card: %-6d, course: %-1d, birthday: %.20s", name, surname, cardNumber, course, format.format(birthsday));
    }

    /**
     * Determines if the specified string is valid card number: contains only
     * one digit character
     *
     * @param number the string to be tested
     * @return true if the specified string is a valid card number, false
     * otherwise.
     */
    public static boolean isValidCardNumber(int number) {
        return number > 0;
    }

    public static boolean isValidCourse(int course) {
        return course > 0 && course <= MAX_COURSE;
    }

    public static boolean isValidBirthday(Date birthsday) {
        return birthsday.before(new Date());
    }

    public static boolean isValidCardNumber(String number) {
        char[] chArray = number.toCharArray();
        for (char ch : chArray) {
            if (!Character.isDigit(ch)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidCourse(String course) {
        try {
            return isValidCourse(Integer.valueOf(course));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidBirthday(String birthsday) {
        try {
            Date date = format.parse(birthsday);
            return date.before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidSurname(String name) {
        return isValidName(name);
    }

    public static boolean isValidName(String name) {
        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
