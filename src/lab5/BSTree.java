package lab5;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 5
 *
 * @param <E>
 * @author Olena Khomenko <br>
 *         <p>
 *         Binary search tree based implementation StudentDictionary Keeps
 *         specified information about students
 *         <p>
 *         each node contains id (number of student's card) and information
 *         about student (name, surname etc.)
 *         <p>
 *         all search, delete and get operation use unique id as a key
 */

public class BSTree<E extends Student> implements StudentDictionary<E> {
    private int size;
    /**
     * root of a tree
     */
    private TreeNode<E> root;

    public BSTree() {

    }

    /**
     * Returns true if this dictionary (binary search tree) contains an student
     * for the specified cardNumber
     *
     * @param cardNumber cardNumber whose presence in this tree is to be tested
     * @return true if this tree contains an student record for the specified
     * cardNumber
     */
    @Override
    public boolean containsKey(int cardNumber) {
        if (cardNumber <= 0) return false;
        TreeNode<E> curr = root;
        while (curr != null) {
            if (curr.getKey() > cardNumber) {
                curr = curr.left;
            } else if (curr.getKey() < cardNumber) {
                curr = curr.right;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the number of nodes in this tree.
     *
     * @return he number of nodes in this tree
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Returns the student to which the specified cardNumber is associated, or
     * null if this tree contains no student for the cardNumber.
     *
     * @param cardNumber the cardNumber whose associated student is to be returned
     * @return the student with the specified cardNumber, or null if this tree
     * contains no student for the cardNumber or cardNumber is invalid
     * (negative or 0)
     */
    @Override
    public E get(int cardNumber) {
        if (cardNumber <= 0) return null;
        TreeNode<E> curr = root;
        while (curr != null) {
            if (curr.getKey() == cardNumber) {
                return curr.st;
            }
            if (curr.getKey() > cardNumber) {
                curr = curr.left;
            } else if (curr.getKey() < cardNumber) {
                curr = curr.right;
            }
        }
        return null;
    }

    private E remove(TreeNode<E> rmNode, boolean decreaseSize) {
        E val = rmNode.getValue();
        if (rmNode.left == null) {
            //remove with 1 child
            replaceSubTree(rmNode, rmNode.right);
        } else if (rmNode.right == null) {
            //remove with 1 child
            replaceSubTree(rmNode, rmNode.left);
        } else {
            //remove with 2 child
            TreeNode<E> newSubRoot = rmNode.right.getMinimum();
            remove(newSubRoot, false);
            newSubRoot.setLeft(rmNode.left);
            newSubRoot.setRight(rmNode.right);
            replaceSubTree(rmNode, newSubRoot);
//            //remove new subRoot from his parent
//            rmNode.setKey(newSubRoot.st);
//            remove(newSubRoot, false);
        }
        if (decreaseSize) size--;
        return val;
    }

    /**
     * Removes the student for this cardNumber from this tree if present.
     *
     * @param cardNumber cardNumber for which student should be removed
     * @return the previous student associated with cardNumber, or null if there
     * was no student for cardNumber.
     */
    @Override
    public E remove(int cardNumber) {
        TreeNode<E> curr = root;
        while (curr != null) {
            if (curr.getKey() > cardNumber) {
                curr = curr.left;
            } else if (curr.getKey() < cardNumber) {
                curr = curr.right;
            } else {
                //find node
                return remove(curr, true);
            }
        }
        return null;
    }

    // use this method when remove nodes
    private void replaceSubTree(TreeNode<E> u, TreeNode<E> v) {
        if (u == root) {
            root = v;
        } else {
            TreeNode<E> par = u.parent;
            if (par != null) {
                if (par.isLeftChild(u)) {
                    par.setLeft(v);
                } else {
                    par.setRight(v);
                }
            }
        }
        u.setLeft(null);
        u.setRight(null);
    }

    private void filter(TreeNode<E> node, int course, Set<Integer> months, ArrayList<TreeNode<E>> filterList) {
        if (node != null) {
            if (months.contains(node.st.getBirthsday().getMonth()) && node.st.getCourse() == course) {
                filterList.add(node);
                System.out.println(node.st);
            }

            filter(node.left, course, months, filterList);
            filter(node.right, course, months, filterList);
        }
    }

    /**
     * Remove from a tree all students that satisfy specified criteria
     *
     * @return number of students was be removed
     */
    public int remove(int course, Integer[] months) {
        int oldSize = size;
        ArrayList<TreeNode<E>> filterList = new ArrayList<TreeNode<E>>();
        HashSet<Integer> monthsSet = new HashSet();
        monthsSet.addAll(Arrays.asList(months));
        // 1) find all nodes witch students satisfy specified removeCriteria
        // save in a list nodes to be removed
        filter(root, course, monthsSet, filterList);

        // 2) call method remove (node) for each node in a list
        for (TreeNode<E> node : filterList) {
            remove(node, true);
        }

        // 3) call size method to check successful removing
        int dif = oldSize - size;
        if (dif == filterList.size()) {
            System.out.println("Success removed " + dif);
        }
        // 4) return difference between old size and new size

        return dif;

    }

    /**
     * Returns true if this dictionary contains no key-value mappings
     *
     * @return true if this dictionary contains no key-value mappings
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Associates the specified student with the specified cardNumber in this
     * dictionary. If the dictionary previously contained a mapping for the
     * cardNumber, the old student is replaced by the specified student.
     *
     * @param num cardNumber with which the specified student is to be
     *            associated
     * @param s   student to be associated with the specified cardNumber
     * @return the previous student associated with cardNumber, or null if there
     * was no mapping for key
     */
    @Override
    public E put(int num, E s) {
        if (root == null) {
            root = new TreeNode<E>(s, null);
            size++;
            return null;
        }
        TreeNode<E> curr = this.root;
        TreeNode<E> par = null;
        while (curr != null) {
            par = curr;
            if (num < curr.getKey()) {
                curr = curr.left;
            } else if (num > curr.getKey()) {
                curr = curr.right;
            } else {
                return curr.setKey(s);
            }
        }
        TreeNode<E> newNode = new TreeNode<E>(s, par);
        if (par.getKey() > num) {
            par.setLeft(newNode);
        } else {
            par.setRight(newNode);
        }
        size++;
        return null;
    }

    /**
     * Outputs dictionary elements in table form
     */
    private void printSubTree(TreeNode<E> node) {
        if (node != null) {
            printSubTree(node.left);
            printSubTree(node.right);
            System.out.println(node);
        }
    }

    @Override
    public void printDictionary() {
        // postorder:
        if (root == null) {
            System.out.println("Dictionary is empty");
        } else {
            printSubTree(root);
        }
    }

    class TreeNode<E extends Student> {
        /**
         * information about student. Instance of class Student
         */
        private E st;

        /**
         * reference to the right node
         */
        private TreeNode<E> right;

        /**
         * reference to the left node
         */
        private TreeNode<E> left;
        /**
         * reference to the parent node
         */

        private TreeNode<E> parent;

        public TreeNode(E e) {
            st = e;
        }

        public TreeNode(E s, TreeNode<E> parent) {
            this(s);
            this.parent = parent;
        }

        public int getKey() {
            return st.getkey();

        }

        public E getValue() {
            return st;
        }

        public TreeNode<E> addLeftChild(E s) {
            this.left = new TreeNode<E>(s);
            return left;
        }

        public TreeNode<E> addRightChild(E s) {
            this.right = new TreeNode<E>(s);
            return left;
        }

        public void setLeft(TreeNode<E> left) {
            this.left = left;
            if (left != null) {
                left.parent = this;
            }
        }

        public void setRight(TreeNode<E> right) {
            this.right = right;
            if (right != null) {
                right.parent = this;
            }
        }

        public boolean isLeftChild(TreeNode<E> node) {
            return node != null && node == left;
        }

        public boolean isRightChild(TreeNode<E> node) {
            return node != null && node == right;
        }

        public TreeNode<E> getMinimum() {
            if (left == null) return this;
            return left.getMinimum();
        }

        public TreeNode<E> getMaximum() {
            if (left == null) return this;
            return left.getMaximum();
        }

        public E setKey(E st) {
            E oldSt = this.st;
            this.st = st;
            return oldSt;
        }

        @Override
        public String toString() {
            return st.toString();
        }

    }

}
