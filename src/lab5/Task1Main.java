package lab5;

import java.io.File;
import java.util.ArrayList;

public class Task1Main {
    // name of file which stores data about students
    private static String fileNameUniq = "studentsUniq.csv";
    private static String fileName = "students.csv";
    // path to the current directory that contains directory "data"
    private static String currenDir = System.getProperty("user.dir")
            + File.separatorChar + "data_lab5";

    private static BSTree<Student> createTree(ArrayList<Student> list) {
        BSTree<Student> tree = new BSTree<>();
        System.out.println("Try to create and fill tree");
        System.out.println("The tree is empty: " + tree.isEmpty());
        for (Student s : list) {
            tree.put(s.getkey(), s);
        }
        System.out.println("Filled tree:");
        tree.printDictionary();
        System.out.println("Tree's size: " + tree.size());

        return tree;
    }

    private static void testContains(StudentDictionary<Student> tree) {
        System.out.println("Test contains: ");
        System.out.println("Contains -5: " + tree.containsKey(-5));
        System.out.println("Contains 18: " + tree.containsKey(18));
        System.out.println("Contains 21: " + tree.containsKey(21));
        System.out.println("Contains 3: " + tree.containsKey(3));
        System.out.println("Contains 10: " + tree.containsKey(10));
        System.out.println("Contains 19: " + tree.containsKey(19));
    }

    private static void testGet(StudentDictionary<Student> tree, ArrayList<Student> list) {
        System.out.println("Test get: ");
        for (Student s : list) {
            System.out.println("Try to get " + s.getkey() + ": " + tree.get(s.getkey()));
        }
        System.out.println("----------------------------------------------");
        System.out.println("Try to get not contained elements");
        System.out.println("Try to get " + 40 + ": " + tree.get(40));
        System.out.println("Try to get " + 0 + ": " + tree.get(0));
        System.out.println("Try to get " + -1 + ": " + tree.get(-1));
        System.out.println("Try to get " + 6 + ": " + tree.get(6));
        System.out.println("Try to get " + 18 + ": " + tree.get(18));
    }

    private static void testRemove(StudentDictionary<Student> tree) {
        System.out.println("Test remove: ");
        System.out.println("Try to remove -1: " + tree.remove(-1));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Try to remove 0: " + tree.remove(0));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Try to remove 30: " + tree.remove(30));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 7: ");
        System.out.println(tree.remove(7));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();
        System.out.println("3 5 8 4 19 17 20 12 10");

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 12: ");
        System.out.println(tree.remove(12));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();
        System.out.println("3 5 8 4 19 17 20 10");

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 4: ");
        System.out.println(tree.remove(4));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();
        System.out.println("3 8 5 19 17 20 10");

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 10(root): ");
        System.out.println(tree.remove(10));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();
        System.out.println("3 8 5 19 20 17");

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 3: ");
        System.out.println(tree.remove(3));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 5: ");
        System.out.println(tree.remove(5));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 8: ");
        System.out.println(tree.remove(8));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 20: ");
        System.out.println(tree.remove(20));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 19: ");
        System.out.println(tree.remove(19));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();

        System.out.println("-------------------------------------------");
        System.out.println("Try to remove 17: ");
        System.out.println(tree.remove(17));
        System.out.println("Tree's size: " + tree.size());
        System.out.println("Dictionary: ");
        tree.printDictionary();
    }

    public static void main(String[] args) {
//        StudentWriter writer = new StudentWriter(fileNameUniq);
//        writer.write(20);


        System.out.println("==========================================================");
        System.out.println("Try with UNIQUE elements");
        // create object of class StudentReader and invoke method read
        StudentReader readerU = new StudentReader(fileNameUniq);
        ArrayList<Student> listU = (ArrayList) readerU.read();
        // TEST dict.put
        // uniq elements
        BSTree<Student> treeU = createTree(listU);
//        System.out.println("13, 11, 9, 14, 10, 8, 7, 4, 5, 12 , 6, 3, 2, 1, 0");
        System.out.println("2, 9 , 8, 13, 11, 10, 7, 3 ,15, 17, 21, 20,19 ,16,14");
        System.out.println("==========================================================");


        System.out.println("==========================================================");
        System.out.println("Try with NOT UNIQUE elements");
        // create object of class StudentReader and invoke method read
        StudentReader reader = new StudentReader(fileName);
        ArrayList<Student> list = (ArrayList) reader.read();
        // TEST dict.put
        // not uniq elements
        StudentDictionary<Student> tree = createTree(list);
        System.out.println("8, 12, 10, 14, 2, 5, 9, 11, 1, 15");
        System.out.println("==========================================================");


        // TEST: dict.contains
        System.out.println("==========================================================");
        testContains(tree);
        System.out.println("==========================================================");


        // TEST: dict.get
        System.out.println("==========================================================");
        testGet(tree, list);
        System.out.println("==========================================================");


        // TEST: dict.remove
        System.out.println("==========================================================");
        testRemove(tree);
        System.out.println("==========================================================");

        System.out.println("==========================================================");
        //TEST: dict.
        System.out.println("Try to remove elements by filter from tree");
        System.out.println("Tree's size: " + treeU.size());
        treeU.remove(2, new Integer[]{0, 1, 11});
        System.out.println("Tree's size: " + treeU.size());
        treeU.printDictionary();
        System.out.println("==========================================================");
    }
}
