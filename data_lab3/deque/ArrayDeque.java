package lab3_class_skeleton.deque;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 3
 * 
 * @author Olena Khomenko <br>
 * 
 *         Resizable-array deque. Implements of the Deque interface. <br>
 *         Array deques have no capacity restrictions; they grow as necessary to
 *         support usage. <br>
 * 
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 * 
 * 
 */

public class ArrayDeque implements Deque {
	// The array in which the elements of the deque are stored.
	String[] queue;

	// The index of the element at the head of the deque
	private int head;

	// The index at which the next element would be added to the tail of the
	// deque
	private int tail;

	// The minimum capacity that we'll use for a newly created deque
	private final int defaultCapacity = 8;

	@Override
	public boolean addFirst(String e) {
		// TODO
		return false;
	}

	@Override
	public boolean addLast(String e) {
		// TODO
		return false;
	}

	@Override
	public String removeFirst() {
		// TODO
		return null;
	}

	@Override
	public String removeLast() {
		// TODO
		return null;
	}

	@Override
	public String getFirst() {
		// TODO
		return null;
	}

	@Override
	public String getLast() {
		// TODO
		return null;
	}

	@Override
	public int size() {
		// TODO
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

}
