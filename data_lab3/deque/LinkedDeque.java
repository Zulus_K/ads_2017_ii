package lab3_class_skeleton.deque;

import lab3_class_skeleton.DLNode;

/**
 * KPI- FPM - PZKS Course: Algorithms and Data Structures (2) Laboratory work 3
 * 
 * @author Olena Khomenko <br>
 * 
 *         Doubly-linked implementation of the Deque interface. <br>
 * 
 *         This class skeleton contains methods with TODO <br>
 *         Implement methods with TODO and write addition methods to support
 *         class functionality.
 */

public class LinkedDeque implements Deque {

	// Pointer to head (first node)
	private DLNode head;

	// Pointer to tail (last node)
	private DLNode tail;

	@Override
	public boolean addFirst(String e) {
		// TODO
		return false;
	}

	@Override
	public boolean addLast(String e) {
		// TODO
		return false;
	}

	@Override
	public String removeFirst() {
		// TODO
		return null;
	}

	@Override
	public String removeLast() {
		// TODO
		return null;
	}

	@Override
	public String getFirst() {
		// TODO
		return null;
	}

	@Override
	public String getLast() {
		// TODO
		return null;
	}

	@Override
	public int size() {
		// TODO
		return 0;
	}

	@Override
	public boolean isEmpty() {
		// TODO
		return false;
	}

}
